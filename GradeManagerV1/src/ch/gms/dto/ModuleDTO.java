package ch.gms.dto;

import java.util.ArrayList;

/*************************************************************************
 * Class to only transfer module data between layers
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class ModuleDTO implements GmsDTO {

	private String number, name, description;
	private ArrayList<ClassDTO> classes;
	private int id;

	/*************************************************************************
	 * Constructor
	 * 
	 * @param number The unique number of the module
	 * @param name The name of the module
	 * @param description The description of the module
	 * @param id The unique ID of the module
	 ************************************************************************/
	public ModuleDTO(String number, String name, String description, int id) {
		this.number = number;
		this.name = name;
		this.description = description;
		this.id = id;
		this.classes = new ArrayList<>();
	}

	/**************************************************************************
	 * Gets the module identification number.
	 *
	 * @return the number
	 **************************************************************************/
	public String getNumber() {
		return number;
	}

	/**************************************************************************
	 * Sets the module identification number.
	 *
	 * @param number the new number
	 **************************************************************************/
	public void setNumber(String number) {
		this.number = number;
	}

	/**************************************************************************
	 * Gets the name.
	 *
	 * @return the name
	 **************************************************************************/
	public String getName() {
		return name;
	}

	/**************************************************************************
	 * Sets the name.
	 *
	 * @param name the new name
	 **************************************************************************/
	public void setName(String name) {
		this.name = name;
	}

	/**************************************************************************
	 * Gets the description.
	 *
	 * @return the description
	 **************************************************************************/
	public String getDescription() {
		return description;
	}

	/**************************************************************************
	 * Sets the description.
	 *
	 * @param description the new description
	 **************************************************************************/
	public void setDescription(String description) {
		this.description = description;
	}

	/**************************************************************************
	 * Gets the classes.
	 *
	 * @return the classes
	 **************************************************************************/
	public ArrayList<ClassDTO> getClasses() {
		return classes;
	}

	/*************************************************************************
	 * Adds a class to this module
	 *
	 * @param schoolClass the school class
	 *************************************************************************/
	public void addClass(ClassDTO classDTO) {
		this.classes.add(classDTO);
	}

	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString() {
		return "Modul " + this.number;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 *************************************************************************/
	public int getId() {
		return id;
	}

}
