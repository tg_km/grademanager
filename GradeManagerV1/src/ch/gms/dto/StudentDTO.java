package ch.gms.dto;

/*************************************************************************
 * Class to only transfer student data between layers
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class StudentDTO implements GmsDTO{
	
	private String firstName, lastName;
	private int studentId, classId;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param firstName The first name of the student
	 * @param lastName The last name of the student
	 * @param id The unique ID of the student
	 * @param classId The unique ID of the class where the student belongs
	 ************************************************************************/
	public StudentDTO(String firstName, String lastName, int id, int classId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentId = id;
		this.classId = classId;
	}

	/**************************************************************************
	 * Gets the first name.
	 *
	 * @return the first name
	 **************************************************************************/
	public String getFirstName() {
		return firstName;
	}

	/**************************************************************************
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 **************************************************************************/
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return studentId;
	}

	/**************************************************************************
	 * Gets the last name.
	 *
	 * @return the last name
	 **************************************************************************/
	public String getLastName() {
		return lastName;
	}

	/**************************************************************************
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 **************************************************************************/
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**************************************************************************
	 * Gets the class id.
	 *
	 * @return the class id
	 **************************************************************************/
	public int getClassId(){
		return this.classId;
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return this.firstName + " " + this.lastName;
	}

}
