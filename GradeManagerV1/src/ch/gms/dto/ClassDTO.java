package ch.gms.dto;

import java.util.ArrayList;

/*************************************************************************
 * Class used only for transfering school class data between layers
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class ClassDTO implements GmsDTO{
	
	private String name;
	private ArrayList<StudentDTO> students;
	private ArrayList<ExamDTO> exams;
	private int id;
	private int lessonId;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param name The name of the class
	 * @param lessonId The lessonID of the class, is -1 when no lesson is
	 * 		  recognizable
	 * @param classId The unique ID of the class
	 ************************************************************************/
	public ClassDTO(String name, int lessonId, int classId) {
		this.name = name;
		this.id = classId;
		this.lessonId = lessonId;
		this.students = new ArrayList<>();
		this.exams = new ArrayList<>();
	}
	
	/**************************************************************************
	 * Sets the name.
	 *
	 * @param name the new name
	 *************************************************************************/
	public void setName(String name){
		this.name = name;
	}
	
	/**************************************************************************
	 * Gets the name.
	 *
	 * @return the name
	 *************************************************************************/
	public String getName(){
		return this.name;
	}
	
	/**************************************************************************
	 * Adds a student to this class.
	 *
	 * @param student the student to be added
	 **************************************************************************/
	public void addStudent(StudentDTO student){
		this.students.add(student);
	}
	
	/**************************************************************************
	 * Gets the students.
	 *
	 * @return the students
	 **************************************************************************/
	public ArrayList<StudentDTO> getStudents(){
		return this.students;
	}

	/**************************************************************************
	 * Adds an exam to this class
	 *
	 * @param exam the exam to be added
	 *************************************************************************/
	public void addExam(ExamDTO exam){
		this.exams.add(exam);
	}
	
	/**************************************************************************
	 * Gets the exams.
	 *
	 * @return the exams
	 **************************************************************************/
	public ArrayList<ExamDTO> getExams() {
		return exams;
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return this.name;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return id;
	}

	/**************************************************************************
	 * Gets the lesson id.
	 *
	 * @return the lesson id
	 **************************************************************************/
	public int getLessonId() {
		return lessonId;
	}
	
}
