package ch.gms.dto;

import java.util.ArrayList;

/*************************************************************************
 * Class to only transfer teacher data between layers
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class TeacherDTO extends UserDTO{
	
	private ArrayList<ModuleDTO> modules;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param firstname The first name of the teacher
	 * @param lastname The last name of the teacher
	 * @param loginSign The login sign of the teacher
	 * @param password The password of the teacher
	 * @param loginId The unique Login ID of the teacher
	 * @param modules All modules belonging to this teacher
	 ************************************************************************/
	public TeacherDTO(String firstname, String lastname, String loginSign, String password,int loginId, ArrayList<ModuleDTO> modules) {
		super(firstname, lastname, loginSign, password, false, loginId);
		this.modules = modules;
	}
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param firstname The first name of the teacher
	 * @param lastname The last name of the teacher
	 * @param loginSign The login sign of the teacher
	 * @param password The password of the teacher
	 * @param loginId The unique Login ID of the teacher
	 ************************************************************************/
	public TeacherDTO(String firstname, String lastname, String loginSign, String password, int loginId) {
		super(firstname, lastname, loginSign, password, false, loginId);
		this.modules = new ArrayList<>();
	}

	/**************************************************************************
	 * Gets the modules.
	 *
	 * @return the modules
	 **************************************************************************/
	public ArrayList<ModuleDTO> getModules() {
		return modules;
	}

	/**************************************************************************
	 * Adds a module to this teacher
	 *
	 * @param modules the modules to be added
	 *************************************************************************/
	public void addModule(ModuleDTO moduleDTO){
		this.modules.add(moduleDTO);
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return super.getFirstname() + " " + super.getLastname();
	}
	
}
