package ch.gms.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*************************************************************************
 * Class to only transfer exam data between layers
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class ExamDTO implements GmsDTO {
	
	private String description;
	private String name;
	private Date date;
	private int lessonId;
	private int id;
	private Map<StudentDTO, Double> grades;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param description The description of the exam
	 * @param name The name of the exam
	 * @param date The date on which the exam has taken place
	 * @param lessonId The lesson ID of the Exam
	 * @param id The unique id of this exam
	 ************************************************************************/
	public ExamDTO(String description,String name, Date date, int lessonId, int id){
		this.grades = new HashMap<>();
		this.description = description;
		this.name = name;
		this.date = date;
		this. lessonId = lessonId;
		this.id = id;
	}
	
	/**************************************************************************
	 * Gets the lesson id.
	 *
	 * @return the lesson id
	 **************************************************************************/
	public int getLessonId() {
		return lessonId;
	}

	/**************************************************************************
	 * Gets the name.
	 *
	 * @return the name
	 **************************************************************************/
	public String getName() {
		return name;
	}

	/**************************************************************************
	 * Sets the name.
	 *
	 * @param name the new name
	 *************************************************************************/
	public void setName(String name) {
		this.name = name;
	}

	/**************************************************************************
	 * Gets the description.
	 *
	 * @return the description
	 **************************************************************************/
	public String getDescription() {
		return description;
	}
	
	/**************************************************************************
	 * Sets the description.
	 *
	 * @param description the new description
	 **************************************************************************/
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**************************************************************************
	 * Gets the date.
	 *
	 * @return the date
	 **************************************************************************/
	public Date getDate() {
		return date;
	}
	
	/**************************************************************************
	 * Gets the grades.
	 * Do not use to get a single grade!
	 *  
	 * @See getGradeForStudent(BOStudent student)
	 * @return the grades
	 **************************************************************************/
	public Map<StudentDTO, Double> getGrades() {
		return grades;
	}
	
	/**************************************************************************
	 * Adds a grade.
	 *
	 * @param student the student
	 * @param value the value
	 *************************************************************************/
	public void addGrade(StudentDTO student, Double value){
		this.grades.put(student, value);
	}
	
	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return id;
	}
}
