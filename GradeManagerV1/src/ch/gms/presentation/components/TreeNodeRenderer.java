package ch.gms.presentation.components;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOStudent;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.ClassDTO;
import ch.gms.dto.ModuleDTO;
import ch.gms.dto.StudentDTO;
import ch.gms.dto.TeacherDTO;
import ch.gms.presentation.util.IconPool;
import ch.gms.presentation.util.IconPool.IconType;

/*****************************************************************
 * Renderer to render different tree icons depending on their
 * user object.
 *
 * @author TIMG
 * @since 16.03.2016
 *****************************************************************/
public class TreeNodeRenderer extends DefaultTreeCellRenderer {
	
	private static final long serialVersionUID = 1L;
	private IconPool iconPool;
	
	/**************************************************************************
	 * Constructor
	 **************************************************************************/
	public TreeNodeRenderer(){
		this.iconPool = IconPool.getInstance();
	}
	
	/**************************************************************************
	 * Decides which icon should be used for the leaf, depending on its
	 * user object.
	 *
	 * @param tree The tree
	 * @param value The user object
	 * @param sel True, when leaf is selected
	 * @param expanded True, when leaf is expanded
	 * @param leaf True, if component is leaf
	 * @param row Row position of leaf
	 * @param hasFocus True if leaf is focused
	 * @return The rendered component
	 **************************************************************************/
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
			boolean leaf, int row, boolean hasFocus){
		
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		if(isSchool(value)){
			setIcon(this.iconPool.get(IconType.SCHOOL_ICON));
		}
		else if(isTeacher(value)){
			setIcon(this.iconPool.get(IconType.TEACHER_ICON));
		}
		else if(isClass(value)){
			setIcon(this.iconPool.get(IconType.CLASS_ICON));
		}
		else if(isModule(value)){
			setIcon(this.iconPool.get(IconType.MODULE_ICON));
		}
		if(isStudent(value)){
			setIcon(this.iconPool.get(IconType.STUDENT_ICON));
		}
		return this;
	}
	
	private boolean isStudent(Object value){
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if(node.getUserObject() instanceof BOStudent || node.getUserObject() instanceof StudentDTO)
			return true;
		return false;
	}
	
	private boolean isModule(Object value){
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if(node.getUserObject() instanceof BOModule || node.getUserObject() instanceof ModuleDTO)
			return true;
		return false;
	}
	
	private boolean isClass(Object value){
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if(node.getUserObject() instanceof BOClass || node.getUserObject() instanceof ClassDTO)
			return true;
		return false;
	}
	
	private boolean isSchool(Object value){
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if(node.getUserObject().equals("GIBM"))
			return true;
		return false;
	}
	
	private boolean isTeacher(Object value){
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if(node.getUserObject() instanceof BOTeacher || node.getUserObject() instanceof TeacherDTO)
			return true;
		return false;
	}
	
}
