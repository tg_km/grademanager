package ch.gms.presentation.components;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.controller.GmsNodeSelectionEvent;
import ch.gms.presentation.util.TreeNodeHelper;

/*****************************************************************
 * Panel to display multiple kinds of hierarchy trees used in the
 * application.
 *
 * @author TIMG
 * @since 07.03.2016
 *****************************************************************/
@SuppressWarnings("unchecked")
public class GmsTreePanel extends JScrollPane{
	
	private static final long serialVersionUID = 1L;
	private JTree tree;
	private DefaultMutableTreeNode root;
	private GmsController controller;
	
	/*************************************************************************
	 * Constructor
	 * Use this constructor if you want to display multiple root's i.e
	 * a collection of teachers.
	 * The type of the Arraylist will internally be detected. Only use lists
	 * containing BOTeachers of BOClasses
	 * 
	 * @param list The collection of entities to display
	 ************************************************************************/
	public GmsTreePanel(ArrayList<?> list){
		this.controller = GmsController.getInstance();
		this.root = new DefaultMutableTreeNode("GIBM");
		
		if(list.get(0) instanceof BOTeacher){
			buildWithTeachers((ArrayList<BOTeacher>) list);
		}else if(list.get(0) instanceof BOClass){
			buildWithClasses((ArrayList<BOClass>) list);
		}else{
			throw new IllegalArgumentException("It's not allowed to pass objects of type " +list.get(0).getClass()
					+". Only use BOTeacher and BOClass objects");
		}
	}
	
	/*************************************************************************
	 * Constructor
	 * Use this constructor if you want to display a single teacher and his
	 * hierarchy of classes, modules and students.
	 * 
	 * @param teacher The teacher to display
	 ************************************************************************/
	public GmsTreePanel(BOTeacher teacher){
		this.controller = GmsController.getInstance();
		this.tree = new JTree(TreeNodeHelper.getTeacherNodeWOStudents(teacher));
		this.tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
				if(node == null) return;
				
				GmsTreePanel.this.controller.notifyEventListener(new GmsNodeSelectionEvent(
						this, 
						(DefaultMutableTreeNode) node.getParent(), 
						node));
			}
		});
		this.tree.setBorder(BorderFactory.createEmptyBorder(5, 7, 3, 0));
		this.tree.setCellRenderer(new TreeNodeRenderer());
		
		this.setViewportView(tree);
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(new Color(146,151,161)),
				null));
		
		this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	}
	
	/*************************************************************************
	 * Builds the tree based on the classes given.
	 * 
	 * @param classes The collection of classes to display
	 ************************************************************************/
	private void buildWithClasses(ArrayList<BOClass> classes){
		for(BOClass clazz : classes){
			root.add(TreeNodeHelper.getClassNode(clazz));
		}
		
		this.tree = new JTree(root);
		this.tree.setBorder(BorderFactory.createEmptyBorder(5, 7, 3, 0));
		this.tree.setCellRenderer(new TreeNodeRenderer());
		
		this.setViewportView(tree);
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(new Color(146,151,161)),
				null));
		this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	}
	
	/*************************************************************************
	 * Builds the tree based on the teachers given.
	 * 
	 * @param teachers The collection of teachers to display
	 ************************************************************************/
	private void buildWithTeachers(ArrayList<BOTeacher> teachers){
		for(BOTeacher teacher : teachers){
			root.add(TreeNodeHelper.getTeacherNodeWStudents(teacher));
		}
		
		this.tree = new JTree(root);
		this.tree.setBorder(BorderFactory.createEmptyBorder(5, 7, 3, 0));
		this.tree.setCellRenderer(new TreeNodeRenderer());
		
		this.setViewportView(tree);
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(new Color(146,151,161)),
				null));
		this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	}
	
	/*************************************************************************
	 * Expands every root node of the tree
	 ************************************************************************/
	public void expandTree(){
		for(int i = 0; i < this.tree.getRowCount(); i++){
			this.tree.expandRow(i);
		}
	}
	
	/*************************************************************************
	 * Rebuilds the tree based on the single teacher given including the
	 * students.
	 * @param teacher The teacher to display
	 ************************************************************************/
	public void rebuildTree(BOTeacher teacher){
		DefaultMutableTreeNode root = TreeNodeHelper.getTeacherNodeWStudents(teacher);
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		model.setRoot(root);
	}
	
	/*************************************************************************
	 * Rebuilds the tree based on the single teacher given without the
	 * students.
	 * @param teacher The teacher to display
	 ************************************************************************/
	public void rebuildTreeWOStudents(BOTeacher teacher){
		DefaultMutableTreeNode root = TreeNodeHelper.getTeacherNodeWOStudents(teacher);
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		model.setRoot(root);
	}
	
	/*************************************************************************
	 * Rebuilds the tree based on the collection of entities given.
	 * Only use collections of BOTeachers or BOClasses!
	 * 
	 * @param list The list to display
	 ************************************************************************/
	public void rebuildTree(ArrayList<?> list){
		if(list.get(0) instanceof BOTeacher){
			rebuildWithTeachers((ArrayList<BOTeacher>) list);
		}else if(list.get(0) instanceof BOClass){
			rebuildWithClasses((ArrayList<BOClass>) list);
		}else{
			throw new IllegalArgumentException("It's not allowed to pass objects of type " +list.get(0).getClass()
					+". Only use BOTeacher and BOClass objects");
		}
	}
	
	/*************************************************************************
	 * Rebuilds the tree based on the collection of classes given.
	 * 
	 * @param classes The classes to display
	 ************************************************************************/
	private void rebuildWithClasses(ArrayList<BOClass> classes){
		this.root = new DefaultMutableTreeNode("GIBM");
		for(BOClass clazz : classes){
			root.add(TreeNodeHelper.getClassNode(clazz));
		}
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		model.setRoot(this.root);
	}
	
	/*************************************************************************
	 * Rebuilds the tree based on list of teachers given.
	 * 
	 * @param teachers The list to display
	 ************************************************************************/
	private void rebuildWithTeachers(ArrayList<BOTeacher> teachers){
		this.root = new DefaultMutableTreeNode("GIBM");
		for(BOTeacher teacher : teachers){
			this.root.add(TreeNodeHelper.getTeacherNodeWStudents(teacher));
		}
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();
		model.setRoot(this.root);
	}
	
	/*************************************************************************
	 * @return The currently selected node of the tree
	 ************************************************************************/
	public DefaultMutableTreeNode getSelectedNote(){
		return (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
	}
	
	/*************************************************************************
	 * @return The root node of the tree
	 ************************************************************************/
	public DefaultMutableTreeNode getRoot(){
		return (DefaultMutableTreeNode)this.tree.getModel().getRoot();
	}
	
	/*************************************************************************
	 * @return the Tree
	 ************************************************************************/
	public JTree getTree(){
		return this.tree;
	}
	
	/*************************************************************************
	 * Selects the node in tree, where the ID of the given object matches
	 * with the ID of the user object in the tree
	 * 
	 * @param objectToBeSelected The object to be selected in the tree. Only objects of
	 * the type BOModule or BOClass are permitted. Every other object will
	 * be ignored.
	 ************************************************************************/
	public void reselectObject(Object objectToBeSelected){
		Enumeration<DefaultMutableTreeNode> tree = getRoot().depthFirstEnumeration();
		while(tree.hasMoreElements()){
			DefaultMutableTreeNode node = tree.nextElement();
			if(objectToBeSelected instanceof BOModule){
				if(node.getUserObject() instanceof BOModule){
					if(((BOModule)node.getUserObject()).getId() == ((BOModule)objectToBeSelected).getId()){
						this.tree.setSelectionPath(new TreePath(node.getPath()));
						break;
					}
				}
			}else if(objectToBeSelected instanceof BOClass){
				if(node.getUserObject() instanceof BOClass){
					if(((BOClass)node.getUserObject()).getLessonId() == ((BOClass)objectToBeSelected).getLessonId()){
						this.tree.setSelectionPath(new TreePath(node.getPath()));
						break;
					}
				}
			}
		}
	}
	
}
