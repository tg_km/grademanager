package ch.gms.presentation.components;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPopupMenu;

/*************************************************************************
 * Button to display a dropdown menu on click
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class DropdownButton extends JButton {
	
	private static final long serialVersionUID = 1L;
	private JPopupMenu popup;
	
	public DropdownButton(String name){
		super(name);
		popup = new JPopupMenu();
		
		this.addActionListener(e -> showMenu());
	}
	
	/*************************************************************************
	 * Adds an element to be shown in the button
	 * 
	 * @param action An abstract action to be added to the button. Its name
	 * 		  will be used as the text displayed in the menu
	 ************************************************************************/
	public void addElement(AbstractAction action){
		popup.add(action);
	}
	
	private void showMenu(){
		int elementCount = popup.getComponentCount();
		int width = (int) Math.floor(this.getWidth() * 1.2);
		popup.setPopupSize(width, elementCount*this.getHeight());
		popup.show(this, 3, this.getHeight());
	}

}
