package ch.gms.presentation.components;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;

import ch.gms.presentation.util.FontPool;
import net.java.balloontip.BalloonTip;
import net.java.balloontip.BalloonTip.AttachLocation;
import net.java.balloontip.BalloonTip.Orientation;
import net.java.balloontip.styles.BalloonTipStyle;
import net.java.balloontip.styles.RoundedBalloonStyle;
import net.java.balloontip.utils.FadingUtils;

/*************************************************************************
 * Helper class to show different kinds of balloon tips in the UI
 * 
 * @author TIMG
 * @since 25.04.2016
 ************************************************************************/
public class BalloonTipHelper{
	
	/*************************************************************************
	 * Shows a normal balloon
	 * 
	 * @param parent The parent component, on which the balloon should be shown
	 * @param text The text to be shown in the balloon
	 ************************************************************************/
	public static void showBalloon(JComponent parent, String text){
		showBalloon(parent, text, 4000);
	}
	
	/*************************************************************************
	 * Shows a normal balloon
	 * 
	 * @param parent The parent component, on which the balloon should be shown
	 * @param text The text to be shown in the balloon
	 * @param delay The time in ms to wait before the balloon tip disappears
	 ************************************************************************/
	public static void showBalloon(JComponent parent, String text, int delay){
		BalloonTip balloonTip = new BalloonTip(parent, text);
		
		BalloonTipStyle style = new RoundedBalloonStyle(5, 5, Color.WHITE, Color.BLACK);
		balloonTip.setStyle(style);
		balloonTip.setFont(FontPool.getInstance().getBoldFont());
		balloonTip.setCloseButton(null);
		
		FadingUtils.fadeInBalloon(balloonTip, 
				e -> fadeOut(balloonTip, delay), 
				500, 24);
	}
	
	/*************************************************************************
	 * Shows an error balloon
	 * 
	 * @param parent The parent component, on which the balloon should be shown
	 * @param text The text to be shown in the balloon
	 * @param orientation An orientation object
	 ************************************************************************/
	public static void showErrorBalloon(JComponent parent, String text, Orientation orientation){
		showErrorBalloon(parent, text, orientation, 4000);
	}
	
	/*************************************************************************
	 * Shows an error balloon
	 * 
	 * @param parent The parent component, on which the balloon should be shown
	 * @param text The text to be shown in the balloon
	 * @param orientation An orientation object
	 * @param delay The time in ms to wait before the balloon tip disappears
	 ************************************************************************/
	public static void showErrorBalloon(JComponent parent, String text, Orientation orientation, int delay){
		
		BalloonTipStyle style = new RoundedBalloonStyle(5, 5, Color.WHITE, Color.RED);
		BalloonTip balloonTip = new BalloonTip(parent, new JLabel(text), style, orientation, AttachLocation.ALIGNED, 20,15,false);
		
		FadingUtils.fadeInBalloon(balloonTip, 
				e -> fadeOut(balloonTip, delay), 
				250, 24);
	}
	
	/*************************************************************************
	 * Constructs a thread, that sleeps for 4 seconds and afterwards let's
	 * the balloon disappear
	 * 
	 * @param balloonTip The balloon tip object to disappear
	 * @param delay The time in ms to wait before the balloon tip disappears
	 ************************************************************************/
	private static void fadeOut(BalloonTip balloonTip, int delay){
		Thread closeBalloon = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(delay);
					FadingUtils.fadeOutBalloon(balloonTip, null, 500, 24);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		closeBalloon.start();
	}

}
