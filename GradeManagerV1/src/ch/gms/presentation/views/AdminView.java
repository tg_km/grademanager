
package ch.gms.presentation.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.TeacherDTO;
import ch.gms.presentation.components.BalloonTipHelper;
import ch.gms.presentation.components.DropdownButton;
import ch.gms.presentation.components.GmsTreePanel;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventListener;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.dialogs.AddCredentialsDialog;
import ch.gms.presentation.views.adminCreateViews.AbstractCreateView;
import ch.gms.presentation.views.adminCreateViews.CreateClassView;
import ch.gms.presentation.views.adminCreateViews.CreateModuleView;
import ch.gms.presentation.views.adminCreateViews.CreateStudentView;
import ch.gms.presentation.views.adminCreateViews.CreateTeacherView;
import ch.gms.presentation.views.adminCreateViews.DummyCreateView;
import ch.gms.presentation.views.adminCreateViews.VIEW_REGISTRY;

/*************************************************************************
 * The main view of the admin user
 * 
 * @author TIMG
 * @since 07.03.2016
 ************************************************************************/
public class AdminView extends AbstractMainView implements GmsEventListener{
	
	private static final long serialVersionUID = 1L;
	
	private GmsTreePanel treeOverviewPanel;
	private GmsTreePanel treeClassesPanel;
	private JList<BOModule> moduleList;
	private JTabbedPane tabbedPane;
	private JPanel editingPanel;
	private JPanel buttonPanel;
	private DropdownButton dropdownButton;
	private JButton saveButton;
	
	private ArrayList<BOTeacher> teachers;
	private ArrayList<BOClass> classes;
	
	private AbstractCreateView activeView;
	
	public AdminView(ArrayList<BOTeacher> allTeachers, ArrayList<BOClass> allClasses){
		this.controller.addEventListener(this);
		this.teachers = allTeachers;
		this.classes = allClasses;
	}
	
	@Override
	public void buildUI() {
		buildDropdownButton();
		
		this.saveButton = new JButton("Speichern");
		this.saveButton.addActionListener(e -> pressSave());
		this.saveButton.setEnabled(false);
		
		this.tabbedPane = new JTabbedPane();
		
		Border listBorder = BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(new Color(146,151,161)), 
						BorderFactory.createEmptyBorder(5, 7, 3, 0));
		
		this.treeClassesPanel = new GmsTreePanel(this.classes);
		this.treeClassesPanel.setPreferredSize(new Dimension(350, 400));
		
		this.moduleList = new JList<>();
		DefaultListModel<BOModule> moduleModel = new DefaultListModel<>();
		this.moduleList.setModel(moduleModel);
		this.moduleList.setBorder(listBorder);
		fillList();
		
		this.treeOverviewPanel = new GmsTreePanel(this.teachers);
		this.treeOverviewPanel.setPreferredSize(new Dimension(350, 400));
		
		this.tabbedPane.addTab("Schulübersicht", this.treeOverviewPanel);
		this.tabbedPane.addTab("Alle Klassen", this.treeClassesPanel);
		this.tabbedPane.addTab("Alle Module", new JScrollPane(this.moduleList));
		
		this.editingPanel = new JPanel(new BorderLayout());
		this.editingPanel.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
		
		this.buttonPanel = new JPanel();
		this.buttonPanel.add(dropdownButton);
		this.buttonPanel.add(saveButton);
		
		switchView(VIEW_REGISTRY.DUMMY_CREATE_VIEW);
		
		this.getContentPane().add(this.tabbedPane, BorderLayout.WEST);
		this.getContentPane().add(this.editingPanel, BorderLayout.CENTER);
		this.getContentPane().add(this.buttonPanel, BorderLayout.SOUTH);
	}
	

	private void switchView(VIEW_REGISTRY view){
		if(this.activeView != null){
			this.controller.removeFromListeners(this.activeView);
			this.activeView = null;
		}
		switch(view){
			case CREATE_CLASS_VIEW:
				this.activeView = new CreateClassView();
				break;
			case CREATE_MODULE_VIEW:
				this.activeView = new CreateModuleView();
				break;
			case CREATE_STUDENT_VIEW:
				this.activeView = new CreateStudentView();
				break;
			case CREATE_TEACHER_VIEW:
				this.activeView = new CreateTeacherView();
				break;
			case DUMMY_CREATE_VIEW:
				this.activeView = new DummyCreateView();
				break;
		}
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.DISABLE_SAVE_BUTTON));
		
		this.activeView.setTitle(view.getTitle());
		this.activeView.buildUI();
		this.editingPanel.removeAll();
		this.editingPanel.add(this.activeView, BorderLayout.CENTER);
		this.editingPanel.validate();
			
	}
	
	private void pressSave() {
		if(this.activeView instanceof CreateTeacherView){
			TeacherDTO newTeacher = (TeacherDTO) this.activeView.getData();
			AddCredentialsDialog dialog = new AddCredentialsDialog();
			String[] loginData = dialog.showDialog();
			newTeacher.setLoginSign(loginData[0]);
			newTeacher.setPassword(loginData[1]);
			if(this.controller.save(newTeacher)){
				BalloonTipHelper.showBalloon(this.saveButton, "Lehrer erfolgreich gespeichert");
			}
			this.treeOverviewPanel.rebuildTree(this.controller.getAllTeachers());
		}else{
			if(this.controller.save(this.activeView.getData())){
				BalloonTipHelper.showBalloon(this.saveButton, "Speichern erfolgreich");
			}
		}
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.DISABLE_SAVE_BUTTON));
		this.activeView.reload(); 
		this.treeOverviewPanel.rebuildTree(this.controller.getAllTeachers());
		this.treeClassesPanel.rebuildTree(this.controller.getAllClassesWithStudents());
		fillList();
	}
	
	@Override
	public void actionPerformed(GmsEvent event) {
		if(event.getType() == GmsEventType.ENABLE_SAVE_BUTTON){
			this.saveButton.setEnabled(true);
			this.validate();
		}
		if(event.getType() == GmsEventType.DISABLE_SAVE_BUTTON){
			this.saveButton.setEnabled(false);
			this.validate();
		}
		if(event.getType() == GmsEventType.RELOAD){
			switchView((VIEW_REGISTRY) event.getAttachement());
		}
	}
	
	@SuppressWarnings("serial")
	private void buildDropdownButton() {
		this.dropdownButton = new DropdownButton("Hinzufügen");
		this.dropdownButton.addElement(new AbstractAction("Klasse") {
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminView.this.switchView(VIEW_REGISTRY.CREATE_CLASS_VIEW);
			}
		});
		this.dropdownButton.addElement(new AbstractAction("Schüler") {
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminView.this.switchView(VIEW_REGISTRY.CREATE_STUDENT_VIEW);
			}
		});
		this.dropdownButton.addElement(new AbstractAction("Modul") {
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminView.this.switchView(VIEW_REGISTRY.CREATE_MODULE_VIEW);
			}
		});
		this.dropdownButton.addElement(new AbstractAction("Lehrer") {
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminView.this.switchView(VIEW_REGISTRY.CREATE_TEACHER_VIEW);
			}
		});
	}
	
	private void fillList(){
		DefaultListModel<BOModule> moduleModel = (DefaultListModel<BOModule>) this.moduleList.getModel();
		moduleModel.removeAllElements();
		this.controller.getAllModules().forEach(module -> moduleModel.addElement(module));
	}
	
}
