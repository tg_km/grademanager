package ch.gms.presentation.views;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.presentation.components.GmsTreePanel;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventListener;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.controller.GmsNodeSelectionEvent;
import ch.gms.presentation.views.panels.ExamPanel;

/*************************************************************************
 * The main view of a teacher
 * 
 * @author TIMG
 * @since 07.03.2016
 ************************************************************************/
public class TeacherView extends AbstractMainView implements GmsEventListener{

	private static final long serialVersionUID = 1L;
	private BOTeacher teacher;
	private GmsTreePanel treePanel;
	private ExamPanel examPanel;
	private boolean examsLoaded;
	
	public TeacherView(BOTeacher teacher){
		this.controller.addEventListener(this);
		this.teacher = teacher;
	}
	
	@Override
	public void buildUI() {
		this.treePanel = new GmsTreePanel(this.teacher);
		this.treePanel.setPreferredSize(new Dimension(350, 400));
		
		JPanel examWrapperP = new JPanel(new BorderLayout());
		examWrapperP.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
		
		this.examPanel = new ExamPanel();
		examWrapperP.add(this.examPanel, BorderLayout.CENTER);
		
		this.getContentPane().add(this.treePanel, BorderLayout.WEST);
		this.getContentPane().add(examWrapperP, BorderLayout.CENTER);
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		if(event instanceof GmsNodeSelectionEvent && !this.examsLoaded){
			this.teacher = this.controller.addExamsToTeacher(this.teacher);
			this.examsLoaded = true;
			Object selectedObject = this.treePanel.getSelectedNote().getUserObject();
			this.treePanel.rebuildTreeWOStudents(this.teacher);
			this.treePanel.reselectObject(selectedObject);
		}
		
		if(event.getType() == GmsEventType.RELOAD_TEACHER){
			this.teacher = this.controller.addExamsToTeacher(this.teacher);
			Object selectedObject = this.treePanel.getSelectedNote().getUserObject();
			this.treePanel.rebuildTreeWOStudents(this.teacher);
			this.treePanel.reselectObject(selectedObject);
		}
		
	}

}
