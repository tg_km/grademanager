package ch.gms.presentation.views.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.models.GmsTableModel;
import ch.gms.presentation.util.FontPool;

/*************************************************************************
 * Table panel to display the actual grades of an exam.
 * 
 * @author TIMG
 * @since 11.05.2016
 ************************************************************************/
public class ExamTablePanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private BOClass schoolClass;
	private GmsController controller;
	private JScrollPane scrollPane;
	private JTable gradesTable;
	private boolean isTableBuilt;
	
	public ExamTablePanel(){
		this.controller = GmsController.getInstance();
		prepareUI();
	}
	
	public void buildTable(BOClass schoolClass){
		this.schoolClass = schoolClass;
		buildUI();
		selectFirstExam();
	}
	
	private void prepareUI() {
		this.setBackground(new Color(214,217,223));
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLineBorder(new Color(146,151,161)));
		
		JLabel placeHolder = new JLabel("W�hlen Sie eine Klasse aus, bevor Daten angezeigt werden k�nnen"); 
		placeHolder.setFont(FontPool.getInstance().getBoldFont());
		placeHolder.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
		
		this.add(placeHolder, BorderLayout.CENTER);
	}

	private void buildUI(){
		TableModel model = new GmsTableModel(schoolClass);
		
		this.gradesTable = new JTable(model);
		this.gradesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.gradesTable.getSelectionModel().addListSelectionListener(e -> fireSummaryPanelUpdate(e));
		this.gradesTable.getTableHeader().setReorderingAllowed(false);
		
		this.gradesTable.setTableHeader(new JTableHeader(this.gradesTable.getColumnModel()){
			private static final long serialVersionUID = 1L;
			@Override
			public Dimension getPreferredSize() {
				Dimension d = super.getPreferredSize();
				d.height += 10;
				return d;
			}	
		});
		
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
		leftRenderer.setHorizontalAlignment(DefaultTableCellRenderer.LEFT);
		this.gradesTable.setDefaultRenderer(Double.class, leftRenderer);
		
		this.gradesTable.getTableHeader().setFont(FontPool.getInstance().getBoldFont());
		this.gradesTable.setRowHeight(30);
		this.gradesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		resizeColumnWidth(this.gradesTable);
		
		this.removeAll();
		this.setBorder(null);
		this.setBackground(Color.WHITE);
		
		this.scrollPane = new JScrollPane(this.gradesTable);
		
		this.add(scrollPane, BorderLayout.CENTER);
		this.validate();
		this.isTableBuilt = true;
	}
	
	private void fireSummaryPanelUpdate(ListSelectionEvent e) {
		if(!e.getValueIsAdjusting()){
			if(this.gradesTable.getModel() instanceof GmsTableModel){
				BOExam exam = ((GmsTableModel)this.gradesTable.getModel()).getSelectedExam(this.gradesTable.getSelectedRow());
				this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.TABLE_SELECTION_CHANGED, exam));
			}
		}
	}

	public void reload(BOClass clazz){
		this.schoolClass = clazz;
		this.gradesTable.setModel(new GmsTableModel(this.schoolClass));
		resizeColumnWidth(this.gradesTable);
	}
	
	public BOClass getData(){
		this.schoolClass = ((GmsTableModel)this.gradesTable.getModel()).getData();
		return this.schoolClass;
	}
	
	public boolean isTableBuilt(){
		return this.isTableBuilt;
	}

	public void clear() {
		if(this.isTableBuilt){
			this.gradesTable.setModel(new DefaultTableModel());
		}
	}
	
	public void setEditable(boolean editable){
		((GmsTableModel)this.gradesTable.getModel()).setEditable(editable);
	}
	
	private void resizeColumnWidth(JTable table) {
	    final TableColumnModel columnModel = table.getColumnModel();
	    for (int column = 0; column < table.getColumnCount(); column++) {
	        int width = 140;
	        for (int row = 0; row < table.getRowCount(); row++) {
	            TableCellRenderer renderer = table.getCellRenderer(row, column);
	            Component comp = table.prepareRenderer(renderer, row, column);
	            width = comp.getPreferredSize().width + 140;
	        }
	        columnModel.getColumn(column).setPreferredWidth(width);
	    }
	}
	
	public GmsTableModel getModel(){
		return (GmsTableModel)this.gradesTable.getModel();
	}
	
	public void selectLastExam(){
		this.gradesTable.setRowSelectionInterval(this.gradesTable.getRowCount()-1, this.gradesTable.getRowCount()-1);
	}
	
	public void stopEditing(){
		if(this.gradesTable.getCellEditor() != null){
			this.gradesTable.getCellEditor().stopCellEditing();
		}
	}

	public void selectFirstExam() {
		if(this.gradesTable.getRowCount() > 0){
			this.gradesTable.setRowSelectionInterval(0, 0);
		}
	}

}
