package ch.gms.presentation.views.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.presentation.util.FontPool;

/*************************************************************************
 * Summary panel for exam data. Gets the data passed by the business
 * objects and displays them.
 * 
 * @author TIMG
 * @since 11.05.2016
 ************************************************************************/
public class ExamSummaryPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private JLabel missed, date, gradesAmount, average, lowest, highest;
	
	
	public ExamSummaryPanel(){
		this.setBackground(Color.LIGHT_GRAY);
		buildUI();
	}
	
	private void buildUI(){
		Border panelBorder = BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(new Color(146,151,161), 2), 
				"Zusammenfassung der Pr�fung", 
				TitledBorder.LEFT, 
				TitledBorder.TOP);
		this.setBorder(panelBorder);
		
		JPanel wrapper = new JPanel(new BorderLayout());
		wrapper.setBorder(BorderFactory.createEmptyBorder(5, 5, 8, 5));
		wrapper.setBackground(Color.WHITE);
		JPanel splitter = new JPanel(new BorderLayout());
		
		JPanel left = new JPanel(new GridLayout(3, 2));
		left.setBackground(Color.WHITE);
		JPanel right = new JPanel(new GridLayout(3, 2));
		right.setBackground(Color.WHITE);
		
		this.missed = new JLabel();
		this.date = new JLabel();
		this.gradesAmount = new JLabel();
		this.average = new JLabel();
		this.lowest = new JLabel();
		this.highest = new JLabel();
		
		Font boldFont = FontPool.getInstance().getBoldFont();
		
		JLabel lDate = new JLabel("Pr�fungsdatum");
		lDate.setFont(boldFont);
		JLabel lAmount = new JLabel("Anzahl Noten:");
		lAmount.setFont(boldFont);
		JLabel lMissed = new JLabel("Anzahl Absenzen:");
		lMissed.setFont(boldFont);
		JLabel lAverage = new JLabel("Durchschnitt:");
		lAverage.setFont(boldFont);
		JLabel lLowest = new JLabel("Tiefste Note:");
		lLowest.setFont(boldFont);
		JLabel lHighest = new JLabel("H�chste Note:");
		lHighest.setFont(boldFont);
		
		left.add(lDate);
		left.add(this.date);
		left.add(lAmount);
		left.add(this.gradesAmount);
		left.add(lMissed);
		left.add(this.missed);
		left.setPreferredSize(new Dimension(370, 80));
		
		right.add(lAverage);
		right.add(this.average);
		right.add(lLowest);
		right.add(this.lowest);
		right.add(lHighest);
		right.add(this.highest);
		right.setPreferredSize(new Dimension(370, 80));
		right.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 2, 0, 0, new Color(146,151,161)),
				BorderFactory.createEmptyBorder(0, 35, 0, 0)));
		
		splitter.add(left, BorderLayout.WEST);
		splitter.add(right, BorderLayout.EAST);
		
		wrapper.add(splitter);
		this.add(wrapper);
		
		this.setBackground(Color.WHITE);
	}
	
	public void load(BOExam boExam, BOClass clazz) {
		if(boExam != null && clazz != null){
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			DecimalFormat format = new DecimalFormat("#.##");
			this.missed.setText(Integer.toString(boExam.getMissed(clazz)));
			this.date.setText(dateFormat.format(boExam.getDate()));
			this.gradesAmount.setText(Integer.toString(boExam.getAmountOfGrades()));
			this.average.setText(format.format(boExam.getAverage()));
			this.lowest.setText(Double.toString(boExam.getLowestGrade()));
			this.highest.setText(Double.toString(boExam.getHighestGrade()));
		}else{
			this.missed.setText("");
			this.date.setText("");
			this.gradesAmount.setText("");
			this.average.setText("");
			this.lowest.setText("");
			this.highest.setText("");
		}
	}

}
