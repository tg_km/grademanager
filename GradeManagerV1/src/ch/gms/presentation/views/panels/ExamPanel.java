package ch.gms.presentation.views.panels;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ch.gms.application.Converter;
import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.presentation.components.BalloonTipHelper;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventListener;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.controller.GmsNodeSelectionEvent;
import ch.gms.presentation.dialogs.AddExamDialog;
import ch.gms.presentation.util.FontPool;

/*************************************************************************
 * Main panel for exam data. Wrapps the table and summary panel and
 * is responsible for event handling.
 * 
 * @author TIMG
 * @since 11.05.2016
 ************************************************************************/
public class ExamPanel extends JPanel implements GmsEventListener {

	private static final long serialVersionUID = 1L;
	private GmsController controller;
	private BOClass clazz;
	private JLabel titleLabel;
	private ExamTablePanel tablePanel;
	private ExamSummaryPanel summaryPanel;

	private JButton addButton;
	private JButton saveButton;
	private JButton editButton;

	public ExamPanel() {
		this.controller = GmsController.getInstance();
		this.controller.addEventListener(this);
		buildUI("Modul ausw�hlen");
	}

	private void buildUI(String moduleTitle) {
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);

		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(new Color(146, 151, 161)),
				BorderFactory.createEmptyBorder(12, 20, 10, 20)));

		titleLabel = new JLabel(moduleTitle, SwingConstants.CENTER);
		titleLabel.setFont(FontPool.getInstance().getTitleFont());
		titleLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 0, 20, 0),
				BorderFactory.createLineBorder(new Color(146, 151, 161), 2)));

		this.tablePanel = new ExamTablePanel();
		this.summaryPanel = new ExamSummaryPanel();

		this.summaryPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0),
				this.summaryPanel.getBorder()));

		JPanel content = new JPanel(new BorderLayout());
		content.add(tablePanel, BorderLayout.CENTER);
		content.add(summaryPanel, BorderLayout.SOUTH);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.WHITE);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));

		this.addButton = new JButton("Pr�fung hinzuf�gen");
		this.addButton.setEnabled(false);
		this.saveButton = new JButton("Speichern");
		this.saveButton.setEnabled(false);
		this.editButton = new JButton("Bearbeiten");
		this.editButton.setEnabled(false);

		addButton.addActionListener(e -> addAction());
		saveButton.addActionListener(e -> saveAction());
		editButton.addActionListener(e -> editAction());

		buttonPanel.add(addButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(editButton);

		this.add(titleLabel, BorderLayout.NORTH);
		this.add(content, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	private void editAction() {
		BalloonTipHelper.showBalloon(this.editButton, "Doppelklicken Sie auf eine Note um sie zu bearbeiten");
		this.editButton.setEnabled(false);
		this.tablePanel.setEditable(true);
		this.saveButton.setEnabled(true);
	}

	private void saveAction() {
		this.tablePanel.stopEditing();
		this.clazz = this.tablePanel.getData();
		this.editButton.setEnabled(true);
		this.saveButton.setEnabled(false);
		this.tablePanel.setEditable(false);
		reload();
		this.controller.save(Converter.convertToDto(this.tablePanel.getData()));
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.RELOAD_TEACHER));
	}

	private void addAction() {
		AddExamDialog dialog = new AddExamDialog(this.tablePanel.getData().getLessonId());
		BOExam exam = dialog.showDialog();	
		if(exam.getName().equals("")){ //dialog returns empty exam if it has been cancelled
			BalloonTipHelper.showBalloon(this.addButton, "Hinzuf�gen abgebrochen...");
			return;
		}
		this.tablePanel.getModel().addExam(exam);
		reload();
		this.tablePanel.selectLastExam();
		BalloonTipHelper.showBalloon(this.saveButton, "Dr�cken Sie \"Bearbeiten\" um die Noten einzutragen.");
	}

	private void reload() {
		if (!this.tablePanel.isTableBuilt()) {
			this.tablePanel.buildTable(this.clazz);
		} else {
			this.tablePanel.reload(this.clazz);
			this.tablePanel.selectFirstExam();
		}

		this.summaryPanel.load(this.tablePanel.getModel().getSelectedExam(0), this.clazz);
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		if (event instanceof GmsNodeSelectionEvent) {
			GmsNodeSelectionEvent selectionEvent = (GmsNodeSelectionEvent) event;
			if (selectionEvent.getSelectedNodeUserObject() instanceof BOModule) {
				this.titleLabel.setText(selectionEvent.getSelectedNodeUserObject().toString());
				this.tablePanel.clear();
				this.addButton.setEnabled(false);
				this.validate();
			} else if (selectionEvent.getSelectedNodeUserObject() instanceof BOClass) {
				this.clazz = (BOClass) selectionEvent.getSelectedNodeUserObject();
				this.titleLabel.setText(selectionEvent.getParentUserObject().toString());
				this.addButton.setEnabled(true);
				reload();
			} else if (selectionEvent.getSelectedNodeUserObject() instanceof BOTeacher) {
				this.tablePanel.clear();
				this.titleLabel.setText("Modul ausw�hlen");
				this.addButton.setEnabled(false);
			}
		}
		if (event.getType() == GmsEventType.TABLE_SELECTION_CHANGED) {
			this.summaryPanel.load((BOExam) event.getAttachement(), this.clazz);
			if (event.getAttachement() != null) {
				this.editButton.setEnabled(true);
			} else {
				this.editButton.setEnabled(false);
			}
		}
	}

}
