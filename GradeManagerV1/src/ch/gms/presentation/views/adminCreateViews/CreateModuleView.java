package ch.gms.presentation.views.adminCreateViews;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ch.gms.dto.GmsDTO;
import ch.gms.dto.ModuleDTO;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventType;

/*************************************************************************
 * The view to create a new module
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class CreateModuleView extends AbstractCreateView{
	
	private static final long serialVersionUID = 1L;
	private JTextField numberField, nameField;
	private JTextArea descText;
	private KeyAdapter checker;
	private boolean saveable = false;

	@Override
	public void buildUI() {
		this.numberField = new JTextField();
		this.nameField = new JTextField();
		this.descText = new JTextArea();
		
		super.addToContent(new JLabel("Modulnummer:"), this.numberField);
		super.addToContent(new JLabel("Bezeichnung:"), this.nameField);
		super.addToContent(new JLabel("Beschreibung:"), this.descText);
		
		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				if(CreateModuleView.this.nameField.getText().length() > 0 && 
						CreateModuleView.this.descText.getText().length() > 0 &&
						CreateModuleView.this.numberField.getText().length() > 0 &&
						!CreateModuleView.this.saveable){	
					CreateModuleView.this.controller.notifyEventListener(new GmsEvent(
							CreateModuleView.this, GmsEventType.ENABLE_SAVE_BUTTON));
					CreateModuleView.this.saveable = true;
					
				}else if(CreateModuleView.this.nameField.getText().length() == 0 || 
						CreateModuleView.this.descText.getText().length() == 0 ||
						CreateModuleView.this.numberField.getText().length() == 0 &&
						CreateModuleView.this.saveable){
					CreateModuleView.this.controller.notifyEventListener(
							new GmsEvent(CreateModuleView.this, GmsEventType.DISABLE_SAVE_BUTTON));
					CreateModuleView.this.saveable = false;
				}
			}
		};
		
		this.nameField.addKeyListener(this.checker);
		this.descText.addKeyListener(this.checker);
		
	}

	@Override
	public GmsDTO getData() {
		return new ModuleDTO(this.numberField.getText(), this.nameField.getText(), this.descText.getText(), -1);
	}

	@Override
	public void reload() {
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.RELOAD, VIEW_REGISTRY.CREATE_MODULE_VIEW));
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		
	}

}
