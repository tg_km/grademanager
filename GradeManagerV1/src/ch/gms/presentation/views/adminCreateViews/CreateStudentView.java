package ch.gms.presentation.views.adminCreateViews;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.dto.GmsDTO;
import ch.gms.dto.StudentDTO;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventType;

/*************************************************************************
 * The view to create a new student
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class CreateStudentView extends AbstractCreateView {

	private static final long serialVersionUID = 1L;
	private JLabel surnameLabel, nameLabel, classLabel;
	private JTextField surnameField, nameField;
	private JComboBox<BOClass> classCombo;
	private KeyAdapter checker;
	private boolean saveable;


	@Override
	public void buildUI() {
		this.surnameLabel = new JLabel("Vorname");
		this.nameLabel = new JLabel("Nachname");
		this.classLabel = new JLabel("Klasse");
		this.surnameField = new JTextField();
		this.nameField = new JTextField();
		BOClass[] allClasses = this.controller.getAllClasses().toArray(new BOClass[this.controller.getAllClasses().size()]);
		this.classCombo = new JComboBox<>(allClasses);
		this.classCombo.setSelectedItem(null);

		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				updateSaveButton();
			}
		};

		this.nameField.addKeyListener(this.checker);
		this.surnameField.addKeyListener(this.checker);
		this.classCombo.addActionListener(e -> updateSaveButton());

		super.addToContent(surnameLabel, surnameField);
		super.addToContent(nameLabel, nameField);
		super.addToContent(classLabel, classCombo);

	}

	private void updateSaveButton() {
		if (isSaveAllowed() != null) {
			if (isSaveAllowed()) {
				this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.ENABLE_SAVE_BUTTON));
			} else {
				this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.DISABLE_SAVE_BUTTON));
			}
		}
	}

	private Boolean isSaveAllowed() {
		if (this.nameField.getText().length() > 0 && this.surnameField.getText().length() > 0
				&& this.classCombo.getSelectedItem() != null && !this.saveable) {
			return true;
		} else if (this.nameField.getText().length() == 0 || this.surnameField.getText().length() == 0
				|| this.classCombo.getSelectedItem() == null && this.saveable) {
			return false;
		}
		return null;
	}

	@Override
	public GmsDTO getData() {
		return new StudentDTO(this.nameField.getText(), this.surnameField.getText(), -1, ((BOClass)this.classCombo.getSelectedItem()).getId());
	}

	@Override
	public void reload() {
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.RELOAD, VIEW_REGISTRY.CREATE_STUDENT_VIEW));
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		
	}

}
