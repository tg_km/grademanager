package ch.gms.presentation.views.adminCreateViews;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;

import ch.gms.application.Converter;
import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.GmsDTO;
import ch.gms.presentation.components.BalloonTipHelper;
import ch.gms.presentation.components.DropdownButton;
import ch.gms.presentation.components.GmsTreePanel;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.dialogs.CreateDialog;
import ch.gms.presentation.dialogs.CreateDialog.CreateDialogType;
import ch.gms.presentation.dialogs.SimpleMessageDialog;
import ch.gms.presentation.dialogs.SimpleMessageDialog.DialogType;
import net.java.balloontip.BalloonTip.Orientation;

/*************************************************************************
 * The view to create a new teacher
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class CreateTeacherView extends AbstractCreateView{
	
	private static final long serialVersionUID = 1L;
	private JLabel surnameLabel, nameLabel;
	private JTextField surnameField, nameField;
	private DropdownButton addButton;
	private JButton moveToTreeButton;
	private JPanel treePanel;
	private GmsTreePanel tree;
	private BOTeacher newTeacher;
	private KeyAdapter checker;
	private boolean saveable = false;
	

	public CreateTeacherView() {
		this.controller.addEventListener(this);
	}
	
	private void createTeacherInTree(){
		this.newTeacher = new BOTeacher(this.nameField.getText(), this.surnameField.getText(), "", "", -1);
		this.tree.rebuildTree(this.newTeacher);
		this.addButton.setEnabled(true);
		this.moveToTreeButton.setEnabled(false);
		this.validate();
	}

	@SuppressWarnings("serial")
	@Override
	public void buildUI() {
		this.surnameLabel = new JLabel("Vorname");
		this.nameLabel = new JLabel("Name");
		this.surnameField = new JTextField();
		this.nameField = new JTextField();
		this.addButton = new DropdownButton("Hinzufügen");
		this.moveToTreeButton = new JButton("In den Tree übernehmen...");
		this.moveToTreeButton.addActionListener(e -> createTeacherInTree());
		this.moveToTreeButton.setEnabled(false);
		this.tree = new GmsTreePanel(new BOTeacher("", "", "", "", 0));
		this.treePanel = new JPanel(new BorderLayout());
		this.treePanel.add(this.tree, BorderLayout.CENTER);
		
		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				if(CreateTeacherView.this.nameField.getText().length() > 0 && 
						CreateTeacherView.this.surnameField.getText().length() > 0 &&
						!CreateTeacherView.this.saveable){	
					CreateTeacherView.this.moveToTreeButton.setEnabled(true);
					CreateTeacherView.this.saveable = true;
					
				}else if(CreateTeacherView.this.nameField.getText().length() == 0 || 
						CreateTeacherView.this.surnameField.getText().length() == 0 &&
						CreateTeacherView.this.saveable){
					CreateTeacherView.this.moveToTreeButton.setEnabled(false);
					CreateTeacherView.this.saveable = false;
				}
			}
		};
		
		this.nameField.addKeyListener(this.checker);
		this.surnameField.addKeyListener(this.checker);
		
		this.addButton.addElement(new AbstractAction("Modul") {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CreateDialog(CreateDialogType.ADD_MODULE);
			}
		});
		this.addButton.addElement(new AbstractAction("Klasse") {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)CreateTeacherView.this.tree.getTree().getLastSelectedPathComponent();
				if(selectedNode != null){
					if(selectedNode.getUserObject() != null && selectedNode.getUserObject() instanceof BOModule){
						new CreateDialog(CreateDialogType.ADD_CLASS);
					}else{
						new SimpleMessageDialog(DialogType.ERROR, "Error", "<html>Selektieren Sie ein Modul,<br> bevor eine Klasse hinzugefügt werden kann.</html>");
					}
				}else{
					new SimpleMessageDialog(DialogType.ERROR, "Error", "<html>Selektieren Sie ein Modul,<br> bevor eine Klasse hinzugefügt werden kann.</html>");
				}
			}
		});
		this.addButton.setEnabled(false);
		
		JPanel buttonWrapper = new JPanel(new BorderLayout());
		buttonWrapper.setBorder(BorderFactory.createEmptyBorder(10,0,5,280));
		buttonWrapper.setBackground(Color.WHITE);
		buttonWrapper.add(this.addButton, BorderLayout.CENTER);
		
		this.treePanel.setBackground(Color.WHITE);
		this.treePanel.add(buttonWrapper, BorderLayout.SOUTH);
		
		this.treePanelPanel.add(this.treePanel, BorderLayout.CENTER);
		this.addToContent(surnameLabel, surnameField);
		this.addToContent(nameLabel, nameField);
		this.addToContent(moveToTreeButton);
	}

	@Override
	public GmsDTO getData() {
		return Converter.convertToDTO(this.newTeacher);
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		if(event.getType() == GmsEventType.ADDED_MODULE){
			BOModule moduleToAdd = (BOModule)event.getAttachement();
			if(!listContainsModule(this.newTeacher.getModules(), moduleToAdd)){
				this.newTeacher.addModule(moduleToAdd);
			}else{
				new SimpleMessageDialog(DialogType.ERROR, "Error", "Das Modul " + moduleToAdd.getNumber() + " existiert bereits ");
				return;
			}
			this.tree.rebuildTree(this.newTeacher);
			this.tree.expandTree();
		}
		if(event.getType() == GmsEventType.ADDED_CLASS){
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)this.tree.getTree().getLastSelectedPathComponent();
			BOModule selectedModule = (BOModule) selectedNode.getUserObject();
			BOClass classToAdd = (BOClass)event.getAttachement();
			if(!moduleContainsClass(selectedModule, classToAdd)){
				this.newTeacher.addClassToModule(selectedModule, (BOClass)event.getAttachement());
			}else{
				new SimpleMessageDialog(DialogType.ERROR, "Error", "<html>Die Klasse " + classToAdd.getName() + " existiert bereits unter dem Modul " + selectedModule.getNumber() + "</html>");
				return;
			}
			this.tree.rebuildTree(this.newTeacher);
			this.tree.expandTree();
			if(!allModulesContainClass()){
				BalloonTipHelper.showErrorBalloon(this.addButton, "Jedes Modul muss mind. eine Klasse enthalten!", Orientation.LEFT_ABOVE);
			}else{
				this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.ENABLE_SAVE_BUTTON));
			}
		}
	}
	
	private boolean listContainsModule(ArrayList<BOModule> list, BOModule bo){
		for(BOModule module : list){
			if(module.getName().equals(bo.getName())){
				return true;
			}
		}
		return false;
	}
	
	private boolean moduleContainsClass(BOModule parent, BOClass child){
		for(BOClass dtoInList : parent.getClasses()){
			if(dtoInList.getName().equals(child.getName())){
				return true;
			}
		}
		return false;
	}
	
	private boolean allModulesContainClass(){
		for(BOModule module : this.newTeacher.getModules()){
			if(module.getClasses() == null || module.getClasses().size() == 0){
				return false;
			}
		}
		return true;
	}

	@Override
	public void reload() {
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.RELOAD, VIEW_REGISTRY.CREATE_TEACHER_VIEW));
	}
	
}
