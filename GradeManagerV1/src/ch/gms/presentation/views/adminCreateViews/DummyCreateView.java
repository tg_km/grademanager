package ch.gms.presentation.views.adminCreateViews;

import javax.swing.JLabel;

import ch.gms.dto.GmsDTO;
import ch.gms.presentation.controller.GmsEvent;

/*************************************************************************
 * The view to be initially shown
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class DummyCreateView extends AbstractCreateView {

	private static final long serialVersionUID = 1L;

	@Override
	public void actionPerformed(GmsEvent event) {
		return;
	}

	@Override
	public void buildUI() {
		this.addToContent(new JLabel("Drücken Sie auf 'Hinzufügen', um etwas hinzuzufügen."));
	}

	@Override
	public GmsDTO getData() {
		return null;
	}

	@Override
	public void reload() {
		return;
	}

}
