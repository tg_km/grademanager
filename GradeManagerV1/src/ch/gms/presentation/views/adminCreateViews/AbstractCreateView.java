package ch.gms.presentation.views.adminCreateViews;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ch.gms.dto.GmsDTO;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.controller.GmsEventListener;
import ch.gms.presentation.util.FontPool;

/*************************************************************************
 * Abstract class to implement all similarities between the different
 * create views.
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public abstract class AbstractCreateView extends JPanel implements GmsEventListener{
	
	private static final long serialVersionUID = 1L;
	private JLabel titleLabel;
	private JPanel contentPanel, fieldPanel, buttonPanel;
	protected JPanel treePanelPanel;
	protected GmsController controller;
	
	public AbstractCreateView(){
		this.controller = GmsController.getInstance();
		this.titleLabel = new JLabel("");
		this.titleLabel.setFont(FontPool.getInstance().getSubTitleFont());
		this.titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 35, 0));
		
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(25, 0, 0, 0, new Color(214,217,223)),
				BorderFactory.createCompoundBorder(
						BorderFactory.createLineBorder(new Color(146,151,161)),
						BorderFactory.createEmptyBorder(30, 25, 20, 20))
				));
				
		this.setBackground(Color.WHITE);
		
		this.contentPanel = new JPanel();
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		this.contentPanel.setBackground(Color.WHITE);
		
		this.fieldPanel = new JPanel();
		this.fieldPanel.setLayout(new BoxLayout(this.fieldPanel, BoxLayout.Y_AXIS));
		this.fieldPanel.setBackground(Color.WHITE);
		
		this.buttonPanel = new JPanel();
		this.buttonPanel.setLayout(new BoxLayout(this.buttonPanel, BoxLayout.Y_AXIS));
		this.buttonPanel.setBackground(Color.WHITE);
		
		this.treePanelPanel = new JPanel();
		this.treePanelPanel.setLayout(new BorderLayout());
		this.treePanelPanel.setBackground(Color.WHITE);
		
		this.contentPanel.add(fieldPanel);
		this.contentPanel.add(buttonPanel);
		
		this.setLayout(new BorderLayout());
		this.add(this.titleLabel, BorderLayout.NORTH);
		this.add(this.contentPanel, BorderLayout.CENTER);
		this.add(this.treePanelPanel, BorderLayout.EAST);
	}
	
	public void setTitle(String title){
		this.titleLabel.setText(title);
		this.validate();
	}
	
	protected void addToContent(JComponent labelComponent, JComponent inputComponent){
		JPanel inputGroup = new JPanel();
		inputGroup.setLayout(new BoxLayout(inputGroup, BoxLayout.Y_AXIS));
		
		labelComponent.setFont(FontPool.getInstance().getBoldFont());
		labelComponent.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));
		labelComponent.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		if(inputComponent instanceof JTextField)
			inputComponent.setMaximumSize(new Dimension(350, 30));
		if(inputComponent instanceof JTextArea){
			inputComponent.setMaximumSize(new Dimension(350, 100));
			((JTextArea) inputComponent).setLineWrap(true);
		}
		if(inputComponent instanceof JComboBox<?>){
			inputComponent.setMaximumSize(new Dimension(120, 25));
		}
		inputComponent.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		inputGroup.add(labelComponent);
		inputGroup.add(inputComponent);
		
		inputGroup.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		inputGroup.setBackground(Color.WHITE);
		
		this.fieldPanel.add(inputGroup);
	}
	
	protected void addToContent(JComponent component){
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
		inputPanel.add(Box.createHorizontalGlue());
		inputPanel.setBackground(Color.WHITE);
		
		component.setAlignmentX(Component.LEFT_ALIGNMENT);
		inputPanel.add(component);
		
		this.buttonPanel.add(inputPanel);
		
	}
	
	public abstract void buildUI();
	public abstract GmsDTO getData();
	public abstract void reload();
	
}
