package ch.gms.presentation.views.adminCreateViews;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;

import ch.gms.dto.ClassDTO;
import ch.gms.dto.GmsDTO;
import ch.gms.presentation.components.BalloonTipHelper;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventType;
import net.java.balloontip.BalloonTip.Orientation;

/*************************************************************************
 * The view to create a new class
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class CreateClassView extends AbstractCreateView {
	
	private static final long serialVersionUID = 1L;
	private JLabel nameLabel;
	private JTextField nameField;
	private KeyAdapter checker;
	private boolean saveable = false;
	
	@Override
	public void buildUI() {
		this.nameLabel = new JLabel("Klassenname");
		this.nameField = new JTextField();
		
		super.addToContent(this.nameLabel, this.nameField);
		
		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				int length = CreateClassView.this.nameField.getText().length();
				if(length > 3 && !CreateClassView.this.saveable && length <= 11){	
					CreateClassView.this.controller.notifyEventListener(new GmsEvent(
							CreateClassView.this, GmsEventType.ENABLE_SAVE_BUTTON));
					CreateClassView.this.saveable = true;
					
				}else if(length <= 3 && CreateClassView.this.saveable){
					CreateClassView.this.controller.notifyEventListener(
							new GmsEvent(CreateClassView.this, GmsEventType.DISABLE_SAVE_BUTTON));
					CreateClassView.this.saveable = false;
				}
				
				if(length >= 12){
					CreateClassView.this.controller.notifyEventListener(
							new GmsEvent(CreateClassView.this, GmsEventType.DISABLE_SAVE_BUTTON));
					BalloonTipHelper.showErrorBalloon(CreateClassView.this.nameField, "Name überschreitet 12 Buchstaben", Orientation.LEFT_BELOW, 1000);
					CreateClassView.this.saveable = false;
				}
			}
		};
		
		this.nameField.addKeyListener(this.checker);
	}

	@Override
	public GmsDTO getData() {
		return new ClassDTO(this.nameField.getText(), -1, -1);
	}

	@Override
	public void reload() {
		this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.RELOAD, VIEW_REGISTRY.CREATE_CLASS_VIEW));
	}

	@Override
	public void actionPerformed(GmsEvent event) {
		
	}

}
