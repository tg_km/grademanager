package ch.gms.presentation.views.adminCreateViews;


/*************************************************************************
 * All create views have to be registered here
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public enum VIEW_REGISTRY {
	
	CREATE_CLASS_VIEW("Klasse erstellen"),
	CREATE_STUDENT_VIEW("Sch�ler erstellen"),
	CREATE_MODULE_VIEW("Modul erstellen"),
	CREATE_TEACHER_VIEW("Lehrer erstellen"),
	DUMMY_CREATE_VIEW("Wilkommen, was wollen Sie tun?");
	
	private String titel;
	
	private VIEW_REGISTRY(String titel){
		this.titel = titel;
	}
	
	public String getTitle(){
		return this.titel;
	}

}
