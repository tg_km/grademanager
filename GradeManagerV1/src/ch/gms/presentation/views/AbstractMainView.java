package ch.gms.presentation.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.dialogs.SimpleMessageDialog;
import ch.gms.presentation.dialogs.SimpleMessageDialog.DialogType;
import ch.gms.presentation.util.FontPool;
import ch.gms.presentation.util.IconPool;
import ch.gms.presentation.util.WindowCenterer;
import ch.gms.presentation.util.IconPool.IconType;

/*************************************************************************
 * Abstract class for all views of the application
 * 
 * @author TIMG
 * @since 07.03.2016
 ************************************************************************/
public abstract class AbstractMainView extends JFrame{
	
	private static final long serialVersionUID = 1L;
	protected GmsController controller;
	private JPanel contentPanel;
	private JMenuBar menuBar;
	private JLabel welcomeLabel;
	private JButton helpButton;
	private JButton logoutButton;
	private JButton exitButton;
	
	private FontPool fontPool;
	private IconPool iconPool;
	
	public AbstractMainView(){
		this.controller = GmsController.getInstance();
		buildFrame();
		setUpFrame();
	}
	
	public abstract void buildUI();
	
	private void buildFrame(){
		this.setTitle("Grade Management System");
		this.setSize(1200, 735);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		WindowCenterer.center(this);
		this.setResizable(false);
	}
	
	@SuppressWarnings("serial")
	private void setUpFrame(){
		fontPool = FontPool.getInstance();
		iconPool = IconPool.getInstance();
		
		ImageIcon frameIcon = (ImageIcon)iconPool.get(IconType.GIBM_ICON);
		this.setIconImage(frameIcon.getImage());
		
		contentPanel = new JPanel();
		contentPanel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
		contentPanel.setLayout(new BorderLayout());
		this.setContentPane(contentPanel);
		
		String welcomeMessage = "Wilkommen, ";
		if(this.controller.getUser().isAdmin()){
			welcomeMessage = "Willkommen, Administrator ";
		}
		welcomeLabel = new JLabel(welcomeMessage + this.controller.getUserDisplayName());
		welcomeLabel.setFont(fontPool.getBoldFont());
		
		menuBar = new JMenuBar();
		menuBar.add(welcomeLabel);
		menuBar.add(Box.createGlue());
		
		Action helpAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new SimpleMessageDialog(DialogType.INFO, "Help mock", "Help would be shown here");
			}
		};
		
		exitButton = new JButton("Exit");
		exitButton.addActionListener(e -> System.exit(1));
		
		logoutButton = new JButton("Logout");
		logoutButton.addActionListener(e -> this.controller.restart());
		
		helpButton = new JButton(helpAction);
		helpButton.setIcon(iconPool.get(IconType.HELP_ICON));
		helpButton.setBorder(BorderFactory.createEmptyBorder());
		
		menuBar.add(logoutButton);
		menuBar.add(exitButton);
		menuBar.add(helpButton);
		
		this.setJMenuBar(menuBar);
		this.setVisible(true);
	}
	
}
