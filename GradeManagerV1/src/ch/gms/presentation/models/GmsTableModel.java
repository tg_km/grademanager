package ch.gms.presentation.models;

import javax.swing.table.AbstractTableModel;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.application.businessObjects.BOStudent;

/*****************************************************************************
 * Generic dialog to display a simple message
 *
 * @author TIMG
 * @since 28.03.2016
 ****************************************************************************/
public class GmsTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	private BOClass schoolClass;
	private String[] columnNames;
	private Class<?>[] columnClass;
	private boolean editable;
	
	/*****************************************************************************
	 * Constructor
	 *
	 * @param schoolClass The schoolClass for which the model should be constructed
	 ****************************************************************************/
	public GmsTableModel(BOClass schoolClass){
		this.schoolClass = schoolClass;
		
		columnNames = new String[this.schoolClass.getStudents().size()+1];
		columnClass = new Class[this.schoolClass.getStudents().size()+1];
		
		columnNames[0] = "Pr�fung";
		int index = 1;
		for(BOStudent student : this.schoolClass.getStudents()){
			columnNames[index] = student.toString();
			index++;
		}
		
		columnClass[0] = String.class;
		for(int i = 0; i<this.schoolClass.getStudents().size(); i++){
			columnClass[i+1] = Double.class;
		}
	}
	
	@Override
	public String getColumnName(int columnIndex){
		return columnNames[columnIndex];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
		return columnClass[columnIndex];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.schoolClass.getExams().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		BOExam row = this.schoolClass.getExams().get(rowIndex);
		
		if(columnIndex == 0){
			return row.getName();
		}else{
			BOStudent student = this.schoolClass.getStudents().get(columnIndex-1);
			return row.getGradeForStudent(student);
		}
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex){
		if((double)value <= 6 && (double)value >= 1){
			BOExam row = this.schoolClass.getExams().get(rowIndex);
			BOStudent student = this.schoolClass.getStudents().get(columnIndex-1);
			row.updateGrade(student, (double)value);
		}else{
			
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex){
		if(columnIndex>0 && this.editable){
			return true;
		}
		return false;
	}
	
	/**************************************************************************
	 * Get's the exam business object at the given row number
	 *
	 * @param rowIndex The position of the wanted exam in the table
	 * @return The exam business object at the given row number
	 **************************************************************************/
	public BOExam getSelectedExam(int rowIndex){
		if(this.schoolClass.getExams().size() > 0){
			if(rowIndex >= 0){
				return this.schoolClass.getExams().get(rowIndex);
			}
		}
		return null;
	}
	
	/**************************************************************************
	 * Specifies if the model should be editable at the moment
	 *
	 * @param editable True when changes should be allowed
	 **************************************************************************/
	public void setEditable(Boolean editable){
		this.editable = editable;
	}
	
	/**************************************************************************
	 * Gets the class business object represented in the model
	 *
	 * @return The BOClass
	 **************************************************************************/
	public BOClass getData(){
		return this.schoolClass;
	}
	
	/**************************************************************************
	 * Adds an exam to the school class
	 *
	 * @param exam The exam to be added
	 **************************************************************************/
	public void addExam(BOExam exam){
		this.schoolClass.addExam(exam);
	}

}
