package ch.gms.presentation.controller;

/*****************************************************************************
 * Enum for all clean up event types
 *
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public enum GmsEventType {
	
	/**
	 * Enables the save button when saving is allowed
	 */
	ENABLE_SAVE_BUTTON, 
	
	/**
	 * Disables the save button when saving is not allowed
	 */
	DISABLE_SAVE_BUTTON, 
	
	/**
	 * Tells a component to reload itself
	 */
	RELOAD,
	
	/**
	 * Notifies that a class has been added to a teacher
	 */
	ADDED_CLASS,
	
	/**
	 * Notifies that a module has been added to a teacher
	 */
	ADDED_MODULE, 
	
	/**
	 * Notifies that the tree selection has been changed
	 */
	TREE_SELECTION_CHANGED,
	
	/**
	 * Notifies that the table selection has been changed
	 */
	TABLE_SELECTION_CHANGED,
	
	/**
	 * Request a reload of the teacher from the database
	 */
	RELOAD_TEACHER;

}
