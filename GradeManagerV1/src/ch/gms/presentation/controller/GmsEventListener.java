package ch.gms.presentation.controller;

import java.util.EventListener;

/*****************************************************************************
 * Interface to specify the actionsPerformed method for CleanUpEvents
 *
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public interface GmsEventListener extends EventListener {
	
	/*************************************************************************
	 * Method to be called by the Controller when delegating an event to
	 * all listeners.
	 * 
	 * @param The thrown GMSEvent
	 ************************************************************************/
	public void actionPerformed(GmsEvent event);

}
