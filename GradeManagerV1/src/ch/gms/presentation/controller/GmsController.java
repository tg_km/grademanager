package ch.gms.presentation.controller;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.event.EventListenerList;

import ch.gms.application.DefaultDataAccessService;
import ch.gms.application.DataAccessService;
import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.application.businessObjects.BOUser;
import ch.gms.application.login.LoginService;
import ch.gms.application.login.LoginFailedException;
import ch.gms.application.login.DefaultLoginService;
import ch.gms.application.util.Pair;
import ch.gms.dto.GmsDTO;
import ch.gms.presentation.views.AdminView;
import ch.gms.presentation.views.TeacherView;

/*****************************************************************************
 * Controller class for the whole application.
 * As there only should be one instance of a controller,
 * this is a singleton and it can only be accessed via getInstance.
 * 
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public class GmsController {
	
	private EventListenerList listeners;
	private AdminView adminView;
	private TeacherView teacherView;
	private BOUser userLoggedIn;
	private DataAccessService dataAccess;
	
	private static GmsController theInstance;

	private GmsController() {
		this.listeners = new EventListenerList();
		this.dataAccess = new DefaultDataAccessService();
	}
	
	/*************************************************************************
	 * Gets the only instance of the controller
	 * 
	 * @return The GMSController instance
	 ************************************************************************/
	public static GmsController getInstance(){
		if(theInstance == null){
			theInstance = new GmsController();
		}
		return theInstance;
	}

	/*************************************************************************
	 * Tries to log in a user with the given credentials.
	 * 
	 * @param userData Pair consisting of the username and the password of
	 *        the user to be logged in
	 * @exception LoginFailedException When no match between username and password
	 * 			  could be made
	 ************************************************************************/
	public void login(Pair<String, String> userData) throws LoginFailedException {
		LoginService loginHandler = new DefaultLoginService();

		this.userLoggedIn = loginHandler.login(userData);
		System.out.println("[" + new Date() + "] User " + this.userLoggedIn.getLoginSign() + " erfolgreich eingeloggt.");

		if (this.userLoggedIn.isAdmin()) {
			this.adminView = new AdminView(getAllTeachers(), getAllClassesWithStudents());
			this.adminView.buildUI();
		} else {
			this.teacherView = new TeacherView(getTeacherFromLogin(this.userLoggedIn.getId()));
			this.teacherView.buildUI();
		}
	}

	/*************************************************************************
	 * Notifies all registered event listeners with the given event
	 * 
	 * @param event The GMSEvent to be thrown
	 ************************************************************************/
	public void notifyEventListener(GmsEvent event) {
		for (GmsEventListener listener : this.listeners.getListeners(GmsEventListener.class)) {
			listener.actionPerformed(event);
		}
	}

	/*************************************************************************
	 * Adds an event listener to the listener list, to be notified of all
	 * incoming GMSEvents
	 * 
	 * @param listener The GMSEventListener to add
	 ************************************************************************/
	public void addEventListener(GmsEventListener listener) {
		this.listeners.add(GmsEventListener.class, listener);
	}

	/*************************************************************************
	 * Removes an event listener from the listener list.
	 * 
	 * @param listener The GMSEventListener to be removed from the list
	 ************************************************************************/
	public void removeFromListeners(GmsEventListener listener) {
		this.listeners.remove(GmsEventListener.class, listener);
	}
	
	/*************************************************************************
	 * Tries to restart the application from the java command line with
	 * the previously given arguments.
	 ************************************************************************/
	public void restart() {
		try {
			//recreating java location and arguments
			String java = System.getProperty("java.home") + "\\bin\\java";
			List<String> vmArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
			StringBuffer vmArgsOneLine = new StringBuffer();
			for (String arg : vmArguments) {
				if (!arg.contains("-agentlib")) {
					vmArgsOneLine.append(arg);
					vmArgsOneLine.append(" ");
				}
			}
			//starting to build run command
			final StringBuffer cmd = new StringBuffer("\"").append(java).append("\" ").append(vmArgsOneLine);
			
			//checking if application has been started from class files or .jar file
			String[] mainCommand = System.getProperty("sun.java.command").split(" ");
			if (mainCommand[0].endsWith(".jar")) {
				cmd.append("-jar " + new File(mainCommand[0]).getPath());
			} else {
				cmd.append("-cp \"").append(System.getProperty("java.class.path")).append("\" ").append(mainCommand[0]);
			}
			for (String arg : mainCommand) {
				cmd.append(" ");
				cmd.append(arg);
			}
			//adds a thread to be run when shutdown is initialized
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					try {
						Runtime.getRuntime().exec(cmd.toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			
			System.exit(0);
		} catch (Exception e) {
			System.err.println("Error while trying to restart the application: " + e.getMessage());
			e.printStackTrace();
		}
	}


	/*************************************************************************
	 * Saves an IGMSDTO 
	 * 
	 * @param data The DTO to be saved
	 ************************************************************************/
	public boolean save(GmsDTO data) {
		return this.dataAccess.save(data);
	}

	/*************************************************************************
	 * @return An arraylist containing all classes from the database without
	 * their students.
	 ************************************************************************/
	public ArrayList<BOClass> getAllClasses() {
		return this.dataAccess.getAllClasses();
	}

	/*************************************************************************
	 * @return An arraylist containing all modules from the database.
	 ************************************************************************/
	public ArrayList<BOModule> getAllModules() {
		return this.dataAccess.getAllModules();
	}

	/*************************************************************************
	 * Gets a BOTeacher by its login ID
	 * 
	 * @param The ID belonging to the desired teacher
	 * @return The BOTeacher matched to the given login ID
	 ************************************************************************/
	public BOTeacher getTeacherFromLogin(int loginId) {
		return this.dataAccess.getTeacherByLoginIdWOExam(loginId);
	}

	/*************************************************************************
	 * Adds all the exam data to an existing teacher business object
	 * 
	 * @param teacher The teacher to who the exams out of the database should
	 * 		  be added.
	 * @return The same teacher with exam data added
	 ************************************************************************/
	public BOTeacher addExamsToTeacher(BOTeacher teacher) {
		return this.dataAccess.addExamsToTeacher(teacher);

	}
	
	/*************************************************************************
	 * Gets all class business objects including their children, but without
	 * their exams
	 * 
	 * @return The found classes
	 ************************************************************************/
	public ArrayList<BOClass> getAllClassesWithStudents(){
		return this.dataAccess.getAllClassesWithStudents();
	}
	
	/*************************************************************************
	 * Checks if a username is available in the database, as one username
	 * can only exists once.
	 * 
	 * @param loginSign The login sign to be tested for uniqueness
	 * @return True if the 
	 ************************************************************************/
	public boolean isAvailable(String loginSign){
		return this.dataAccess.isAvailable(loginSign);
	}
	
	/*************************************************************************
	 * @return The BO of the user currently logged in
	 ************************************************************************/
	public BOUser getUser() {
		return this.userLoggedIn;
	}
	
	/*************************************************************************
	 * @return The display name of the user currently logged in in this 
	 * 		   session. Can be null if no user is logged in.
	 ************************************************************************/
	public String getUserDisplayName() {
		if (userLoggedIn != null) {
			return userLoggedIn.getFirstname() + " " + userLoggedIn.getLastname();
		} else {
			return null;
		}
	}
	
	/*************************************************************************
	 * @return An arraylist containing all teachers from the database.
	 ************************************************************************/
	public ArrayList<BOTeacher> getAllTeachers() {
		return dataAccess.getAllTeachers();
	}
	
}
