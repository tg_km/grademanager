package ch.gms.presentation.controller;

import javax.swing.tree.DefaultMutableTreeNode;

/*****************************************************************************
 * Event to be thrown when the tree selection has changed
 *
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public class GmsNodeSelectionEvent extends GmsEvent {
	
	private static final long serialVersionUID = 1L;
	private DefaultMutableTreeNode parent;
	private DefaultMutableTreeNode node;

	/*************************************************************************
	 * Constructor
	 * 
	 * @param source The object from which the event has been thrown
	 * @param parent The parent of the selected node
	 * @param node The selected node
	 ************************************************************************/
	public GmsNodeSelectionEvent(Object source, DefaultMutableTreeNode parent, DefaultMutableTreeNode node) {
		super(source, GmsEventType.TREE_SELECTION_CHANGED);
		this.parent = parent;
		this.node = node;
	}
	
	/*************************************************************************
	 * Gets the selected node
	 * 
	 * @return the selected DefaultMutableTreeNode
	 ************************************************************************/
	public DefaultMutableTreeNode getSelectedNode(){
		return this.node;
	}
	
	/*************************************************************************
	 * Gets the parent of the selected node
	 * 
	 * @return the parent of the selected DefaultMutableTreeNode
	 ************************************************************************/
	public DefaultMutableTreeNode getParent(){
		return this.parent;
	}
	
	/*************************************************************************
	 * Gets the user object of the selected node
	 * 
	 * @return the user object of the selected node.
	 ************************************************************************/
	public Object getSelectedNodeUserObject(){
		return this.node.getUserObject();
	}
	
	/*************************************************************************
	 * Gets the user object of the parent of the selected node
	 * 
	 * @return the user object of the parent of the selected node.
	 ************************************************************************/
	public Object getParentUserObject(){
		return this.parent.getUserObject();
	}

}
