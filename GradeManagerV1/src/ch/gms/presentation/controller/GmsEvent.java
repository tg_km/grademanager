package ch.gms.presentation.controller;

import java.util.EventObject;

/*****************************************************************************
 * Event to be thrown for all controller actions
 *
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public class GmsEvent extends EventObject{

	private static final long serialVersionUID = 1L;
	private GmsEventType type;
	private Object attachement;

	/*************************************************************************
	 * Constructs a GMSEvent
	 * 
	 * @param source The object from where the event will be thrown
	 * @param type The GMSEventType of the event to be thrown
	 ************************************************************************/
	public GmsEvent(Object source, GmsEventType type) {
		super(source);
		this.type = type;
	}
	
	/*************************************************************************
	 * Constructs a GMSEvent with an attachement transfered with it
	 * 
	 * @param source The object from where the event will be thrown
	 * @param type The GMSEventType of the event to be thrown
	 * @param attachement An attachement
	 ************************************************************************/
	public GmsEvent(Object source, GmsEventType type, Object attachement){
		this(source, type);
		this.attachement = attachement;
	}
	
	/*************************************************************************
	 * Gets the GMSEventType of the event
	 * 
	 * @return Returns the type of the event
	 ************************************************************************/
	public GmsEventType getType(){
		return this.type;
	}
	
	/*************************************************************************
	 * Gets the attachement object of this event. Can be NULL if no attachement
	 * has been added to the event.
	 * 
	 * @return Returns the attachement of the event
	 ************************************************************************/
	public Object getAttachement(){
		return this.attachement;
	}

}
