package ch.gms.presentation.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.controller.GmsEvent;
import ch.gms.presentation.controller.GmsEventListener;
import ch.gms.presentation.controller.GmsEventType;
import ch.gms.presentation.util.WindowCenterer;

/*************************************************************************
 * Dialog to add either an existing module or class to a new teacher
 * 
 * @author TIMG
 * @since 16.04.2016
 ************************************************************************/
public class CreateDialog extends JDialog implements GmsEventListener{
	
	public enum CreateDialogType{
		ADD_MODULE, 
		ADD_CLASS
	}
	
	private static final long serialVersionUID = 1L;
	private GmsController controller;
	private CreateDialogType type;
	private JScrollPane scrollPane;
	private DefaultListModel<Object> listModel;
	private JList<Object> list;
	private JButton okButton;
	
	/**************************************************************************
	 * Constructor
	 *
	 * @param type The type of the dialog
	 **************************************************************************/
	public CreateDialog(CreateDialogType type){
		this.type = type;
		this.controller = GmsController.getInstance();
		this.controller.addEventListener(this);
		this.setLayout(new BorderLayout());
		this.setSize(500, 300);
		this.setVisible(true);
		buildUI();
		WindowCenterer.center(this);
	}
	
	private void buildUI(){
		JPanel contentWrapper = new JPanel(new BorderLayout());
		contentWrapper.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		this.setBackground(Color.WHITE);
		this.scrollPane = new JScrollPane()
				;
		this.listModel = new DefaultListModel<>();
		this.list = new JList<>();
		this.list.setModel(this.listModel);
		
		this.list.addListSelectionListener(e -> controlButton());
		
		this.okButton = new JButton("OK");
		this.okButton.setEnabled(false);
		this.okButton.addActionListener(e -> addAndClose());
		this.okButton.setMaximumSize(new Dimension(30,20));
		
		if(this.type == CreateDialogType.ADD_MODULE){
			this.setTitle("Modul hinzufügen");	
			this.controller.getAllModules().forEach(module -> this.listModel.addElement(module));
		}else if(this.type == CreateDialogType.ADD_CLASS){
			this.setTitle("Klasse hinzufügen");
			this.controller.getAllClasses().forEach(clazz -> this.listModel.addElement(clazz));
		}
		
		this.scrollPane.setViewportView(this.list);
		contentWrapper.add(this.scrollPane, BorderLayout.CENTER);
		contentWrapper.add(this.okButton, BorderLayout.SOUTH);
		
		this.add(contentWrapper, BorderLayout.CENTER);
	}
	
	private void controlButton() {
		if(this.list.getSelectedValue() != null){
			this.okButton.setEnabled(true);
		}else{
			this.okButton.setEnabled(false);
		}
	}

	private void addAndClose(){
		if(this.type == CreateDialogType.ADD_CLASS){
			this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.ADDED_CLASS, this.list.getSelectedValue()));
		}else if(this.type == CreateDialogType.ADD_MODULE){
			this.controller.notifyEventListener(new GmsEvent(this, GmsEventType.ADDED_MODULE, this.list.getSelectedValue()));
		}
		this.dispose();
	}
	
	@Override
	public void actionPerformed(GmsEvent event) {
		
	}
	
}
