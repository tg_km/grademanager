package ch.gms.presentation.dialogs;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ch.gms.presentation.util.IconPool;
import ch.gms.presentation.util.WindowCenterer;
import ch.gms.presentation.util.IconPool.IconType;

/*****************************************************************************
 * Generic dialog to display a simple message
 *
 * @author TIMG
 * @since 10.12.2015
 ****************************************************************************/
public class SimpleMessageDialog extends JDialog{
	
	private static final long serialVersionUID = 1L;

	public enum DialogType{INFO, ERROR, WARNING};
	
	private Icon icon;
	private IconPool iconPool;
	
	/**************************************************************************
	 * Constructor
	 *
	 * @param type The type of the dialog
	 * @param title The title
	 * @param message The message to be shown
	 **************************************************************************/
	public SimpleMessageDialog(DialogType type, String title, String message){
		iconPool = IconPool.getInstance();
		
		switch (type) {
		case INFO:
			this.icon = iconPool.get(IconType.INFO_ICON);
			break;
		case ERROR:
			this.icon = iconPool.get(IconType.ERROR_ICON);
			break;
		case WARNING:
			this.icon = iconPool.get(IconType.WARNING_ICON);
			break;
		default:
			this.icon = new ImageIcon();
		}
		buildFrame(title, message);
		this.pack();
		WindowCenterer.center(this);
	}
	
	private void buildFrame(String title, String message){
		this.setTitle(title);
		this.setLayout(new BorderLayout());
		
		JLabel label = new JLabel(message);
		label.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 10));
		
		JButton button = new JButton("Ok");
		button.addActionListener(e -> dispose());
		
		JPanel contentWrapper = new JPanel(new BorderLayout());
		contentWrapper.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		
		contentWrapper.add(new JLabel(this.icon), BorderLayout.WEST);
		contentWrapper.add(label, BorderLayout.CENTER);
		contentWrapper.add(button, BorderLayout.SOUTH);
		
		this.add(contentWrapper);
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}

}
