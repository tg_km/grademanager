package ch.gms.presentation.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import ch.gms.presentation.components.BalloonTipHelper;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.util.FontPool;
import ch.gms.presentation.util.WindowCenterer;
import net.java.balloontip.BalloonTip.Orientation;

/*************************************************************************
 * Dialog to add login credentials to a teacher
 * 
 * @author KENMAU
 * @since 11.05.2016
 ************************************************************************/
public class AddCredentialsDialog extends JDialog{

	private static final long serialVersionUID = 1L;
	private GmsController controller;
	private JTextField signField;
	private JPasswordField passwordField, confirmPasswordField;
	private JLabel warningLabel;
	private KeyAdapter checker;
	private JButton okButton;

	public AddCredentialsDialog() {
		this.controller = GmsController.getInstance();
		this.setLayout(new BorderLayout());
		this.setModalityType(ModalityType.APPLICATION_MODAL); // stellt sicher,
																// dass
																// Applikation
																// geblockt ist,
																// bis Dialog
																// geschlossen
																// wird
		this.setTitle("Logindaten spezifizieren");
		this.setSize(500, 280);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		buildUI();
		WindowCenterer.center(this);
	}

	private void buildUI() {
		JPanel contentWrapper = new JPanel(new BorderLayout());
		contentWrapper.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		JLabel warning = new JLabel(
				"<html>Definieren Sie bitte die Zugangsdaten, <br>mit welchen sich der zu erstellende Lehrer anmelden werden kann.</html>");

		JPanel labels = new JPanel(new GridLayout(0, 1));
		JPanel fields = new JPanel(new GridLayout(0, 1));
		JPanel fieldsWrapper = new JPanel(new BorderLayout());

		this.signField = new JTextField();
		this.signField.setPreferredSize(new Dimension(200, 30));

		this.passwordField = new JPasswordField();
		this.passwordField.setPreferredSize(new Dimension(200, 30));

		this.confirmPasswordField = new JPasswordField();
		this.confirmPasswordField.setPreferredSize(new Dimension(200, 30));

		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				String loginSign = AddCredentialsDialog.this.signField.getText();
				String pass1 = new String(AddCredentialsDialog.this.passwordField.getPassword());
				String pass2 = new String(AddCredentialsDialog.this.confirmPasswordField.getPassword());

				if (loginSign.length() > 3 & pass1.length() > 3 && pass2.length() > 3) {
					if (pass1.equals(pass2)) {
						AddCredentialsDialog.this.okButton.setEnabled(true);
						AddCredentialsDialog.this.warningLabel.setText("");
					} else {
						AddCredentialsDialog.this.okButton.setEnabled(false);
						AddCredentialsDialog.this.warningLabel.setText("Die beiden Passw�rter stimmen nicht �berein");
							}					
				} else {
					AddCredentialsDialog.this.okButton.setEnabled(false);
					AddCredentialsDialog.this.warningLabel.setText("");
				}
			}
		};

		this.okButton = new JButton("Speichern");
		this.okButton.addActionListener(e -> close());
		this.okButton.setEnabled(false);

		this.warningLabel = new JLabel("");
		this.warningLabel.setFont(FontPool.getInstance().getBoldFont());
		this.warningLabel.setForeground(Color.RED);
		this.warningLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));

		labels.add(new JLabel("Benutzername:"));
		fields.add(this.signField);
		labels.add(new JLabel("Passwort"));
		fields.add(this.passwordField);
		labels.add(new JLabel("Passwort best�tigen:"));
		fields.add(this.confirmPasswordField);

		Arrays.asList(fields.getComponents()).forEach(comp -> comp.addKeyListener(this.checker));

		JPanel content = new JPanel(new BorderLayout());
		content.add(labels, BorderLayout.WEST);
		content.add(fields, BorderLayout.EAST);
		fieldsWrapper.add(content, BorderLayout.NORTH);
		fieldsWrapper.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));

		JPanel footerWrapper = new JPanel(new BorderLayout());
		footerWrapper.add(warningLabel, BorderLayout.NORTH);
		footerWrapper.add(okButton, BorderLayout.SOUTH);

		contentWrapper.add(warning, BorderLayout.NORTH);
		contentWrapper.add(fieldsWrapper, BorderLayout.CENTER);
		contentWrapper.add(footerWrapper, BorderLayout.SOUTH);

		this.add(contentWrapper, BorderLayout.CENTER);
	}

	public String[] showDialog() {
		setVisible(true);
		return new String[] { this.signField.getText(), new String(this.passwordField.getPassword()) };
	}

	private void close() {
		if (this.controller.isAvailable(signField.getText())) {
			this.setVisible(false);
			this.dispose();
		}else {
			BalloonTipHelper.showErrorBalloon(signField, "Dieser Loginname existiert bereits", Orientation.LEFT_ABOVE, 1000);
		}
	}
}
