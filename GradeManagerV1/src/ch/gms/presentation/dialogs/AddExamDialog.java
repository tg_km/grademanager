package ch.gms.presentation.dialogs;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import ch.gms.application.businessObjects.BOExam;
import ch.gms.presentation.util.DateLabelFormatter;
import ch.gms.presentation.util.WindowCenterer;

/*************************************************************************
 * Dialog to add a new exam
 * 
 * @author KENMAU
 * @since 15.05.2016
 ************************************************************************/
public class AddExamDialog extends JDialog{

	private static final long serialVersionUID = 1L;
	private JTextField examNameTF;
	private JTextField examDescriptionTF;
	private JButton okButton;
	private Date selectedDate;
	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private KeyAdapter checker;
	private int lessonId;
	
	/**************************************************************************
	 * Constructor
	 *
	 * @param lessonId The lessonId of the new exam
	 **************************************************************************/
	public AddExamDialog(int lessonId) {
		this.lessonId = lessonId;
		this.setLayout(new BorderLayout());
		this.setModalityType(ModalityType.APPLICATION_MODAL); // stellt sicher,
																// dass
																// Applikation
																// geblockt ist,
																// bis Dialog
																// geschlossen
																// wird
		this.setTitle("Neue Pr�fung anlegen");
		this.setSize(480, 240);
		buildUI();
		WindowCenterer.center(this);
	}

	private void buildUI() {
		JPanel contentWrapper = new JPanel(new BorderLayout());
		contentWrapper.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		JPanel labels = new JPanel(new GridLayout(0, 1));
		JPanel fields = new JPanel(new GridLayout(0, 1));
		JPanel fieldsWrapper = new JPanel(new BorderLayout());

		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");

		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		this.examNameTF = new JTextField();

		this.examDescriptionTF = new JTextField();
		
		this.checker = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				selectedDate = (Date) datePicker.getModel().getValue();
				String examName = AddExamDialog.this.examNameTF.getText();
				String examDescription = AddExamDialog.this.examDescriptionTF.getText();
				
				if(examName.length() > 0 && examDescription.length() > 0 && selectedDate.toString().length() > 0){
						AddExamDialog.this.okButton.setEnabled(true);
				}else{
					AddExamDialog.this.okButton.setEnabled(false);
				}
				
			}
		};

		this.okButton = new JButton("Speichern");
		this.okButton.addActionListener(e -> close());
		this.okButton.setEnabled(false);

		labels.add(new JLabel("Pr�fungsname:"));
		labels.add(new JLabel("Datum:"));
		labels.add(new JLabel("Pr�fungsbeschreibung:"));
		fields.add(this.examNameTF);
		fields.add(datePicker);
		fields.add(this.examDescriptionTF);
		
		Arrays.asList(fields.getComponents()).forEach(comp -> comp.addKeyListener(this.checker));
		
		JPanel content = new JPanel(new BorderLayout());
		content.add(labels, BorderLayout.WEST);
		content.add(fields, BorderLayout.EAST);
		fieldsWrapper.add(content, BorderLayout.NORTH);
		fieldsWrapper.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));

		JPanel footerWrapper = new JPanel(new BorderLayout());
		footerWrapper.add(okButton, BorderLayout.SOUTH);

		contentWrapper.add(fieldsWrapper, BorderLayout.CENTER);
		contentWrapper.add(footerWrapper, BorderLayout.SOUTH);

		this.add(contentWrapper, BorderLayout.CENTER);
	}

	/**************************************************************************
	 * Method to be called when dialog should be shown
	 *
	 * @return The data entered in the dialog, in form of a new BOExam
	 **************************************************************************/
	public BOExam showDialog() {
		setVisible(true);
		selectedDate = (Date) datePicker.getModel().getValue();
		String desc = this.examDescriptionTF.getText();
		String name = this.examNameTF.getText();
		return new BOExam(desc, name, selectedDate, lessonId, 0);
	}

	private void close() {
		this.setVisible(false);
		this.dispose();
	}
}
