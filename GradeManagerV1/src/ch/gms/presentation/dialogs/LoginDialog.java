package ch.gms.presentation.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import ch.gms.application.login.LoginFailedException;
import ch.gms.application.util.Pair;
import ch.gms.presentation.controller.GmsController;
import ch.gms.presentation.util.FontPool;
import ch.gms.presentation.util.IconPool;
import ch.gms.presentation.util.WindowCenterer;
import ch.gms.presentation.util.IconPool.IconType;

/*************************************************************************
 * Login dialog of the application
 * 
 * @author TIMG
 * @since 07.03.2016
 ************************************************************************/
public class LoginDialog extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private GmsController controller;
	private JPanel header;
	private JPanel content;
	private JPanel labels;
	private JPanel fields;
	private JPanel footer;
	private JLabel wrongPw;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton cancelButton;
	private IconPool iconPool;
	
	/**************************************************************************
	 * Constructor
	 **************************************************************************/
	public LoginDialog(){
		this.controller = GmsController.getInstance();
		buildUI();
	}
	
	private void buildUI(){
		this.iconPool = IconPool.getInstance();
		
		ImageIcon frameIcon = (ImageIcon)iconPool.get(IconType.GIBM_ICON);
		this.setIconImage(frameIcon.getImage());
		
		
		this.setSize(400, 250);
		this.setLayout(new BorderLayout());
		this.getRootPane().setBorder(BorderFactory.createEmptyBorder(10, 12, 10, 10));
		this.setTitle("Login");
		
		this.usernameField = new JTextField();
		this.usernameField.setPreferredSize(new Dimension(200, 28));
		this.passwordField = new JPasswordField();
		this.passwordField.setPreferredSize(new Dimension(200, 28));
		this.loginButton = new JButton("Login");
		this.cancelButton = new JButton("Abbrechen");
		
		this.header = new JPanel(new BorderLayout());
		this.content = new JPanel(new BorderLayout());
		this.labels = new JPanel(new GridLayout(0,1));
		this.fields = new JPanel(new GridLayout(0,1));
		this.footer = new JPanel();
		this.footer.setLayout(new BoxLayout(this.footer, BoxLayout.LINE_AXIS));
		this.footer.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		
		this.wrongPw = new JLabel("");
		this.wrongPw.setFont(FontPool.getInstance().getBoldFont());
		this.wrongPw.setForeground(Color.RED);
		
		JPanel contentWrapper = new JPanel(new BorderLayout());
		contentWrapper.add(this.content, BorderLayout.NORTH);
		contentWrapper.add(this.wrongPw, BorderLayout.SOUTH);
		
		JLabel logoLabel = new JLabel(iconPool.get(IconType.GIBM_ICON));
		logoLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		
		this.passwordField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				super.focusGained(e);
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						passwordField.selectAll();
					}
				});
			}
		});
		
		usernameField.addActionListener(e -> login(usernameField.getText(), passwordField.getPassword()));
		passwordField.addActionListener(e -> login(usernameField.getText(), passwordField.getPassword()));
		loginButton.addActionListener(e -> login(usernameField.getText(), passwordField.getPassword()));
		
		this.cancelButton.addActionListener(e -> System.exit(0));
		
		this.header.add(logoLabel, BorderLayout.CENTER);
		
		this.labels.add(new JLabel("Benutzername:"));
		this.fields.add(this.usernameField);
		this.labels.add(new JLabel("Passwort"));
		this.fields.add(this.passwordField);
		
		this.content.add(this.labels, BorderLayout.WEST);
		this.content.add(this.fields, BorderLayout.EAST);
		
		
		this.footer.add(Box.createHorizontalGlue());
		this.footer.add(this.loginButton);
		this.footer.add(this.cancelButton);
		
		this.add(this.header, BorderLayout.NORTH);
		this.add(contentWrapper, BorderLayout.CENTER);
		this.add(this.footer, BorderLayout.SOUTH);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		WindowCenterer.center(this);
	}
	
	private void login(String username, char[] password){
		String passwordString = new String(password);
		Pair<String, String> loginData = new Pair<>(username, passwordString);
		try{
			this.controller.login(loginData);
			this.dispose();
		} catch (LoginFailedException lfe){
			System.err.println("[" + new Date() +"] "+ lfe.getMessage());
			this.wrongPw.setText(lfe.getMessage());
			this.validate();
		}
	}

}
