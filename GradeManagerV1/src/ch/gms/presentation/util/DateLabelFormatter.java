package ch.gms.presentation.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

/*****************************************************************************
 * Custom formatter for to format data from the date picker.
 *
 * @author KENMAU
 * @since 25.05.2016
 ****************************************************************************/
public class DateLabelFormatter extends AbstractFormatter {

	private static final long serialVersionUID = 1L;
	private static final String PATTERN = "dd.MM.yyyy";
    private SimpleDateFormat dateFormatter = new SimpleDateFormat(PATTERN);

    @Override
    public Object stringToValue(String text) throws ParseException {
        return this.dateFormatter.parseObject(text);
    }

    @Override
    public String valueToString(Object value) throws ParseException {
        if (value != null) {
            Calendar cal = (Calendar) value;
            return this.dateFormatter.format(cal.getTime());
        }

        return "";
    }

}
