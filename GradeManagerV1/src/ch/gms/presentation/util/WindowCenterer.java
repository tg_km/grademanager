package ch.gms.presentation.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

/*****************************************************************************
 * Helper class used for centering window component
 *
 * @author TIMG
 * @since 01.06.2016
 ****************************************************************************/
public class WindowCenterer {
	
	/**************************************************************************
	 * Sets the window location of a given component to the calculated
	 * middle of the screen.
	 * 
	 * @param comp The component to be centered
	 *************************************************************************/
	public static void center(Component comp){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
		Point newLocation = new Point(middle.x - (comp.getWidth() / 2), 
		                              middle.y - (comp.getHeight() / 2));
		comp.setLocation(newLocation);
	}
	

}
