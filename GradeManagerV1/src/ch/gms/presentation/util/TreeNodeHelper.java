package ch.gms.presentation.util;

import javax.swing.tree.DefaultMutableTreeNode;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOStudent;
import ch.gms.application.businessObjects.BOTeacher;

/*****************************************************************************
 * Helper class used for building the needed DefaultMutableTreeNodes based
 * on our business objects.
 *
 * @author TIMG
 * @since 20.04.2016
 ****************************************************************************/
public class TreeNodeHelper {
	
	/**************************************************************************
	 * Builds the root node based on a teacher with all students
	 * 
	 * @param teacher The root teacher
	 * @return The root node of the teacher
	 *************************************************************************/
	public static DefaultMutableTreeNode getTeacherNodeWStudents(BOTeacher teacher){
		DefaultMutableTreeNode teacherNode = new DefaultMutableTreeNode(teacher);
		
		for(BOModule module : teacher.getModules()){
			DefaultMutableTreeNode moduleNode = new DefaultMutableTreeNode(module);
			
			for(BOClass schoolClass : module.getClasses()){
				DefaultMutableTreeNode classNode = new DefaultMutableTreeNode(schoolClass);
				
				for(BOStudent student : schoolClass.getStudents()){
					DefaultMutableTreeNode studentNode = new DefaultMutableTreeNode(student);
					classNode.add(studentNode);
				}
				moduleNode.add(classNode);
			}
			teacherNode.add(moduleNode);
		}
		return teacherNode;
	}
	
	/**************************************************************************
	 * Builds the root node based on a teacher without any students
	 * 
	 * @param teacher The root teacher
	 * @return The root node of the teacher
	 *************************************************************************/
	public static DefaultMutableTreeNode getTeacherNodeWOStudents(BOTeacher teacher){
		DefaultMutableTreeNode teacherNode = new DefaultMutableTreeNode(teacher);
		
		for(BOModule module : teacher.getModules()){
			DefaultMutableTreeNode moduleNode = new DefaultMutableTreeNode(module);
			
			for(BOClass schoolClass : module.getClasses()){
				DefaultMutableTreeNode classNode = new DefaultMutableTreeNode(schoolClass);
				moduleNode.add(classNode);
			}
			teacherNode.add(moduleNode);
		}
		return teacherNode;
	}

	/**************************************************************************
	 * Builds a class node including its students
	 * 
	 * @param clazz The class business object
	 * @return The root node of the class
	 *************************************************************************/
	public static DefaultMutableTreeNode getClassNode(BOClass clazz) {
		DefaultMutableTreeNode classNode = new DefaultMutableTreeNode(clazz);
		
		for(BOStudent student : clazz.getStudents()){
			DefaultMutableTreeNode studentNode = new DefaultMutableTreeNode(student);
			classNode.add(studentNode);
		}
		
		return classNode;
	}

}
