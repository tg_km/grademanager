package ch.gms.presentation.util;

import java.awt.Font;

/*****************************************************************************
 * Helper class for all fonts used. Every font is only instantiated once in
 * this class, which helps saving resources.
 *
 * @author TIMG
 * @since 10.12.2016
 ****************************************************************************/
public class FontPool {
	
	private static FontPool theInstance = new FontPool();
	
	private Font boldFont = new Font("SansSerif", Font.BOLD, 12);
	private Font subTitleFont = new Font("SansSerif", Font.BOLD, 24);
	private Font titleFont = new Font("SansSerif", Font.BOLD, 28);
	private Font standardFont = new Font("SansSerif", Font.PLAIN, 12);

	/**************************************************************************
	 * Private constructor
	 *************************************************************************/
	private FontPool(){}
	
	/**************************************************************************
	 * Gets the only instance of the font pool in the application
	 *
	 * @return The only instance of the font pool
	 **************************************************************************/
	public static FontPool getInstance(){
		return theInstance;
	}
	
	/**************************************************************************
	 * Gets the bold font.
	 *
	 * @return the bold font
	 *************************************************************************/
	public Font getBoldFont(){
		return boldFont;
	}
	
	/**************************************************************************
	 * Gets the sub title font.
	 *
	 * @return the sub title font
	 *************************************************************************/
	public Font getSubTitleFont(){
		return subTitleFont;
	}
	
	/**************************************************************************
	 * Gets the title font.
	 *
	 * @return the title font
	 **************************************************************************/
	public Font getTitleFont(){
		return titleFont;
	}
	
	/**************************************************************************
	 * Gets the standard font.
	 *
	 * @return the standard font
	 **************************************************************************/
	public Font getStandardFont() {
		return standardFont;
	}
	
}
