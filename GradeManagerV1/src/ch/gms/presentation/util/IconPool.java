package ch.gms.presentation.util;

import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/*****************************************************************************
 * Helper class for all icons used. Every icon is only instantiated once in
 * this class, which helps saving resources.
 *
 * @author TIMG
 * @since 01.06.2016
 ****************************************************************************/
public class IconPool {
	
	 /****************************************************************
	 * Enum defining the different icons and their path
	 *
	 * @author TIMG
	 * @since 17.06.2016
	 *****************************************************************/
	public enum IconType{
		HELP_ICON("resources/icons/questionMark.png"),
		CLASS_ICON("resources/icons/classIcon.png"),
		STUDENT_ICON("resources/icons/studentIcon.png"),
		MODULE_ICON("resources/icons/moduleIcon.png"),
		SCHOOL_ICON("resources/icons/schoolIcon.png"),
		TEACHER_ICON("resources/icons/teacherIcon.png"),
		GIBM_ICON("resources/symbols/GIBM-rgb.png"),
		ERROR_ICON("resources/symbols/error.png"),
		WARNING_ICON("resources/symbols/warning.png"),
		INFO_ICON("resources/symbols/information.png");
		
		private String url;
		
		private IconType(String url){
			this.url = url;
		}
		
		/****************************************************************
		 * Gets the path to the icon
		 * 
		 * @return The path as a string
		 ****************************************************************/
		public String getUrl(){
			return this.url;
		}
	}
	
	private static IconPool theInstance;
	private Map<IconType, Icon> pool;
	
	/**************************************************************************
	 * Private constructor
	 *************************************************************************/
	private IconPool(){
		pool = new HashMap<>();
	}
	
	/**************************************************************************
	 * Gets the only instance of the icon pool in the application
	 *
	 * @return The only instance of the font pool
	 **************************************************************************/
	public static IconPool getInstance(){
		if(theInstance == null){
			theInstance = new IconPool();
		}
		return theInstance;
	}
	
	/***************************************************************************
	 * Gets a icon from the pool.
	 *
	 * @param type The enum entry of the wanted icon
	 * @return The found icon
	 **************************************************************************/
	public Icon get(IconType type){
		Icon icon = this.pool.get(type);
		
		if(icon == null){
			icon = new ImageIcon(type.getUrl());
			this.pool.put(type, icon);
		}
		
		return icon;
	}
	
}
