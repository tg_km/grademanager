package ch.gms.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import ch.gms.application.login.DefaultLoginService;
import ch.gms.application.login.LoginFailedException;
import ch.gms.application.login.LoginService;
import ch.gms.application.util.Pair;

/***********************************************************************
 * Class to test the login functionality of the login service
 * 
 * @author KENMAU
 * @since 25.05.2016
 **********************************************************************/
public class LoginTest {

	LoginService loginService;
	final String adminPw = "admin";
	final String adminLoginSign = "admin";
	final String userPw = "zauro";
	final String userLoginSign = "zauro";
	final Pair<String, String> loginAdmin = new Pair<>(adminLoginSign, adminPw);
	final Pair<String, String> loginUser = new Pair<>(userLoginSign, userPw);
	
	/***********************************************************************
	 * Instantiates the login service used by the test
	 **********************************************************************/
	@Before
	public void setUpBefore(){
		this.loginService = new DefaultLoginService();
	}

	/***********************************************************************
	 * Tests if a login with wrong credentials fails as expected
	 **********************************************************************/
	@Test(expected = LoginFailedException.class )
	public void testWrongUser() throws LoginFailedException  {
		loginService.login(new Pair<String, String>("12345","12345"));
	}
	
	/***********************************************************************
	 * Tests if the correct user dto is returned from the handler
	 **********************************************************************/
	@Test
	public void testLogin() {
		try {
			//admin tests
			assertTrue("User null, altough the user exitst", !loginService.login(loginAdmin).equals(null));
			assertTrue("User should be admin", loginService.login(loginAdmin).isAdmin());
			
			//user tests
			assertTrue("User null, although the user exists", !loginService.login(loginUser).equals(null));
			assertTrue("User should not be admin", !loginService.login(loginUser).isAdmin());
		} catch (LoginFailedException e) {
			fail();
			e.printStackTrace();
		}
	}
}
