package ch.gms.test;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.gms.application.Converter;
import ch.gms.application.DefaultDataAccessService;
import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOStudent;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.ClassDTO;
import ch.gms.dto.ExamDTO;
import ch.gms.dto.StudentDTO;
import ch.gms.persistence.DefaultDataAccessObject;
import ch.gms.persistence.IOManager;

/***********************************************************************
 * Class to test if an exam can be saved and reloaded successfully
 * 
 * @author KENMAU
 * @since 25.05.2016
 **********************************************************************/
public class ExamTest {

	ClassDTO classDTO;
	BOClass boClass;
	ExamDTO examDTO;
	BOExam boExam;
	int testExamId;
	int testModuleID = 58; // Modul 226
	int testClassID = 2357; // IAP14A (LessonID)
	StudentDTO studentDTO;
	BOStudent boStudent;
	IOManager iom;
	DefaultDataAccessObject gmdao;
	DefaultDataAccessService gmsda;
	BOTeacher boTeacher;

	/***********************************************************************
	*  Delete the insertet test Exam
	**********************************************************************/
	@Before
	public void setupExamSaveTest() {
		gmsda = new DefaultDataAccessService();
		boTeacher = loadTeacherWithExams();
		iom = IOManager.getInstance();
		boExam = new BOExam("Description", "JUnit Test", new Date(), testClassID, -1);
	}

	/***********************************************************************
	*  load the teacher and adds all his students/exams 
	**********************************************************************/
	public BOTeacher loadTeacherWithExams() {
		BOTeacher teacher = gmsda.getTeacherByLoginIdWOExam(30);
		teacher = gmsda.addExamsToTeacher(teacher);
		return teacher;
	}
	
	/***********************************************************************
	*  Test if a saved Exam can be fetched from the DB
	**********************************************************************/
	@Test
	public void testExamSave() {
		boolean sucessful = saveExam();
		boTeacher = loadTeacherWithExams();
		ArrayList<BOModule> modules = boTeacher.getModules();
		for (BOModule module : modules) {
			int modId = module.getId();
			if (modId == testModuleID) {
				ArrayList<BOClass> classes = module.getClasses();
				for (BOClass clazz : classes) {
					int clazzID = clazz.getLessonId();
					if (clazzID == testClassID) {
						assertTrue("Test not found",
								clazz.getExams().stream().anyMatch(exam -> exam.getName().equals("JUnit Test")&&sucessful));
					}
				}
			}
		}
	}
	
	/***********************************************************************
	*  Save an exam to the Database
	*  
	*  @return True if the save succeeded
	**********************************************************************/
	public boolean saveExam() {
		ArrayList<BOModule> modules = boTeacher.getModules();
		for (BOModule module : modules) {
			int modId = module.getId();
			if (modId == testModuleID) {
				ArrayList<BOClass> classes = module.getClasses();
				for (BOClass clazz : classes) {
					int clazzID = clazz.getLessonId();
					if (clazzID == testClassID) {
						clazz.addExam(boExam);
						boolean save = gmsda.save(Converter.convertToDto(clazz));
						return save;
					}
				}
			}
		}
		return false;
	}
	
	/***********************************************************************
	 *  Delete the insertet test Exam
	 **********************************************************************/
	@After
	public void cleanupDB() {
		Connection conn = null;

		try {
			conn = iom.getConnection();
			PreparedStatement stmt = conn
					.prepareStatement("DELETE FROM pruefung WHERE NAME='JUnit Test' ORDER BY ID DESC LIMIT 1");
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null){
					conn.close();
				}
			} catch (SQLException e) {
			}
		}
	}
}
