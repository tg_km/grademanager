package ch.gms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import ch.gms.presentation.dialogs.LoginDialog;

/*************************************************************************
 * Entry-point of the application
 * 
 * @author TIMG
 * @since 10.03.2016
 ************************************************************************/
public class Main{
	
	/**************************************************************************
	 * Main method, starts the application
	 **************************************************************************/
	public static void main(String[] args){
		try {
			start();
		} catch (Exception e) {
			String message = e.getMessage();
			JOptionPane.showMessageDialog(new JDialog(), message, "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private static void start() throws Exception{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run(){
				
				UIManager.put("nimbusRed", new Color(202, 55, 49, 1));
				UIManager.put("Table.alternateRowColor", new Color(200,204,213));
				UIManager.put("Table.gridColor", new Color(146,151,161));
				UIManager.put("Table.disabled", false);
				UIManager.put("Table.showGrid", true);
				UIManager.put("Table.intercellSpacing", new Dimension(1,1));
				
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				    if ("Nimbus".equals(info.getName())) {
				        try {
							UIManager.setLookAndFeel(info.getClassName());
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (InstantiationException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (UnsupportedLookAndFeelException e) {
							e.printStackTrace();
						}
				        break;
				    }
				}
				new LoginDialog();
			}
		});
	}

}
