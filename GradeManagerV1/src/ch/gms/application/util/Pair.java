package ch.gms.application.util;

/*************************************************************************
 * Container to group two values, that belong together
 * 
 * @author TIMG
 * @since 22.02.2016
 ************************************************************************/
public class Pair<T, E>{
	
	private T firstValue;
	private E secondValue;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param firstValue The first of the two values
	 * @param secondValue The second of the two values
	 ************************************************************************/
	public Pair(T firstValue, E secondValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}

	/**************************************************************************
	 * Gets the first value.
	 *
	 * @return the first value
	 **************************************************************************/
	public T getFirstValue() {
		return firstValue;
	}

	/**************************************************************************
	 * Sets the first value.
	 *
	 * @param firstValue the new first value
	 **************************************************************************/
	public void setFirstValue(T firstValue) {
		this.firstValue = firstValue;
	}

	/**************************************************************************
	 * Gets the second value.
	 *
	 * @return the second value
	 **************************************************************************/
	public E getSecondValue() {
		return secondValue;
	}

	/**************************************************************************
	 * Sets the second value.
	 *
	 * @param secondValue the new second value
	 **************************************************************************/
	public void setSecondValue(E secondValue) {
		this.secondValue = secondValue;
	}
	
}
