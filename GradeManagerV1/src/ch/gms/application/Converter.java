package ch.gms.application;

import java.util.Map;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOExam;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOStudent;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.application.businessObjects.BOUser;
import ch.gms.dto.ClassDTO;
import ch.gms.dto.ExamDTO;
import ch.gms.dto.ModuleDTO;
import ch.gms.dto.StudentDTO;
import ch.gms.dto.TeacherDTO;
import ch.gms.dto.UserDTO;

/*************************************************************************
 * Helper class used for conversion between BO and DTO's
 * 
 * @author TIMG/KENMAU
 * @since 10.03.2016
 ************************************************************************/
public class Converter {
	
	/***********************************************************************
	 * Converts a BOTeacher to a TeacherDTO
	 * 
	 * @param boTeacher The teacher business object to be converted
	 * @return The converted TeacherDTO
	 **********************************************************************/
	public static TeacherDTO convertToDTO(BOTeacher teacher){
		TeacherDTO teacherDTO = new TeacherDTO(teacher.getFirstname(), teacher.getLastname(), 
				teacher.getLoginSign(), teacher.getPassword(), teacher.getId());
				
		for(BOModule module : teacher.getModules()){
			ModuleDTO moduleDTO = new ModuleDTO(module.getNumber(), module.getName(), module.getDescription(), module.getId());
			for(BOClass schoolClass : module.getClasses()){
				ClassDTO classDTO = new ClassDTO(schoolClass.getName(), schoolClass.getLessonId(), schoolClass.getId());
				for(BOStudent student : schoolClass.getStudents()){
					StudentDTO studentDTO = new StudentDTO(student.getFirstName(), student.getLastName(), student.getId(), student.getClassId());
					classDTO.addStudent(studentDTO);
				}
				for(BOExam exam : schoolClass.getExams()){
					ExamDTO examDTO = new ExamDTO(exam.getDescription(), exam.getName(), exam.getDate(), exam.getLessonId(), exam.getId());
					for(Map.Entry<BOStudent, Double> entry : exam.getGrades().entrySet()){
						examDTO.addGrade(new StudentDTO(entry.getKey().getFirstName(), entry.getKey().getLastName(), entry.getKey().getId(), entry.getKey().getClassId()), entry.getValue());
					}
					classDTO.addExam(examDTO);
				}
				moduleDTO.addClass(classDTO);
			}
			teacherDTO.addModule(moduleDTO);
		}
		return teacherDTO;
	}
	
	/***********************************************************************
	 * Converts a TeacherDTO to a BOTeacher
	 * 
	 * @param teacherDto The teacher transfer object to be converted
	 * @return The converted BOTeacher
	 **********************************************************************/
	public static BOTeacher convertToBO(TeacherDTO teacherDTO){
		BOTeacher boTeacher = new BOTeacher(teacherDTO.getLastname(), teacherDTO.getFirstname(),
				teacherDTO.getLoginSign(), teacherDTO.getPassword(), teacherDTO.getId());
		
		for(ModuleDTO moduleDTO : teacherDTO.getModules()){
			BOModule boModule = new BOModule(moduleDTO.getNumber(), moduleDTO.getName(), moduleDTO.getDescription(), moduleDTO.getId());
			for(ClassDTO classDTO : moduleDTO.getClasses()){
				BOClass boClass = new BOClass(classDTO.getName(), classDTO.getLessonId(), classDTO.getId());
				for(StudentDTO studentDTO : classDTO.getStudents()){
					BOStudent boStudent = new BOStudent(studentDTO.getFirstName(), studentDTO.getLastName(), studentDTO.getId(), studentDTO.getClassId());
					boClass.addStudent(boStudent);
				}
				for(ExamDTO examDTO : classDTO.getExams()){
					BOExam boExam = new BOExam(examDTO.getDescription(),examDTO.getName(), examDTO.getDate(), examDTO.getLessonId(), examDTO.getId());
					for(Map.Entry<StudentDTO, Double> entry : examDTO.getGrades().entrySet()){
						boExam.addGrade(new BOStudent(entry.getKey().getFirstName(), entry.getKey().getLastName(), entry.getKey().getId(), entry.getKey().getClassId()), entry.getValue());
					}
					boClass.addExam(boExam);
				}
				boModule.addClass(boClass);
			}
			boTeacher.addModule(boModule);
		}
		return boTeacher;
	}
	
	/***********************************************************************
	 * Converts a UserDTO to a User business object
	 * 
	 * @param userDto The User transfer object to be converted
	 * @return The converted User business object
	 **********************************************************************/
	public static BOUser convertToBO(UserDTO userDto){
		return new BOUser(userDto.getLastname(), userDto.getFirstname(), 
				userDto.getLoginSign(), userDto.getPassword(), userDto.isAdmin(), userDto.getId());
	}
	
	/***********************************************************************
	 * Converts a ClassDTO to a class business object. 
	 * Ignores any exams/grades belonging to this class
	 * 
	 * @param classDto The class transfer object to be converted
	 * @return The converted class business object
	 **********************************************************************/
	public static BOClass convertToBo(ClassDTO classDto){
		BOClass schoolClass = new BOClass(classDto.getName(), classDto.getLessonId(), classDto.getId());
		classDto.getStudents().forEach(student -> schoolClass.addStudent(Converter.convertToBo(student)));
		return schoolClass;
	}
	
	/***********************************************************************
	 * Converts a StudentDTO to a student business object. 
	 * 
	 * @param studentDto The class transfer object to be converted
	 * @return The converted student business object
	 **********************************************************************/
	public static BOStudent convertToBo(StudentDTO studentDto){
		return new BOStudent(studentDto.getFirstName(), studentDto.getLastName(), studentDto.getId(), studentDto.getClassId());
	}
	
	/***********************************************************************
	 * Converts a ModuleDTO to a module business object. 
	 * 
	 * @param moduleDto The module transfer object to be converted
	 * @return The converted module business object
	 **********************************************************************/
	public static BOModule convertToBO(ModuleDTO moduleDto){
		return new BOModule(moduleDto.getNumber(), moduleDto.getName(), moduleDto.getDescription(), moduleDto.getId());
	}
	
	public static ExamDTO convertToDto(BOExam exam){
		ExamDTO dto = new ExamDTO(exam.getDescription(), exam.getName(), exam.getDate(), exam.getLessonId(), exam.getId());
		for(Map.Entry<BOStudent, Double> entry : exam.getGrades().entrySet()){
			dto.addGrade(new StudentDTO(entry.getKey().getFirstName(), entry.getKey().getLastName(), entry.getKey().getId(), entry.getKey().getClassId()), entry.getValue()
					);
		}
		return dto;
	}
	
	/***********************************************************************
	 * Converts a class business object to ClassDTO 
	 * Includes any exams/grades belonging to this class
	 * 
	 * @param clazz The class business object to be converted
	 * @return The converted class transfer object
	 **********************************************************************/
	public static ClassDTO convertToDto(BOClass clazz){
		ClassDTO classDto = new ClassDTO(clazz.getName(), clazz.getLessonId(), clazz.getId());
		clazz.getExams().forEach(exam -> classDto.addExam(Converter.convertToDto(exam)));
		return classDto;
	}

}
