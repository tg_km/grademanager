package ch.gms.application;

import java.util.ArrayList;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.GmsDTO;
import ch.gms.persistence.DefaultDataAccessObject;
import ch.gms.persistence.DataAccessObject;

/*************************************************************************
 * The only way to access data stored in the database
 * 
 * @author KENMAU
 * @since 11.03.2016
 ************************************************************************/
public class DefaultDataAccessService implements DataAccessService {

	private DataAccessObject dao;
	
	public DefaultDataAccessService() {
		dao = new DefaultDataAccessObject();
	}

	@Override
	public BOTeacher addExamsToTeacher(BOTeacher teacher) {
		teacher.getModules().forEach(module -> module.getClasses().forEach(clazz -> clazz.resetExams()));
		return Converter.convertToBO(this.dao.addExamsToTeacher(Converter.convertToDTO(teacher)));
	}

	@Override
	public BOTeacher getTeacherByLoginIdWOExam(int loginId) {
		return Converter.convertToBO(this.dao.getTeacherByLoginIdWOExam(loginId));
	}

	@Override
	public ArrayList<BOTeacher> getAllTeachers() {
		ArrayList<BOTeacher> teachers = new ArrayList<>();
		this.dao.getAllTeachers().forEach(dto -> teachers.add(Converter.convertToBO(dto)));
		return teachers;
	}

	@Override
	public boolean save(GmsDTO dto) {
		return this.dao.save(dto);
	}

	@Override
	public ArrayList<BOClass> getAllClasses() {
		ArrayList<BOClass> classes = new ArrayList<>();
		this.dao.getAllClasses().forEach(dto -> classes.add(Converter.convertToBo(dto)));
		return classes;
	}

	@Override
	public ArrayList<BOModule> getAllModules() {
		ArrayList<BOModule> modules = new ArrayList<>();
		this.dao.getAllModules().forEach(dto -> modules.add(Converter.convertToBO(dto)));
		return modules; 
	}

	@Override
	public ArrayList<BOClass> getAllClassesWithStudents() {
		ArrayList<BOClass> classes = new ArrayList<>();
		this.dao.getAllClassesWithStudents().forEach(dto -> classes.add(Converter.convertToBo(dto)));
		return classes;
	}

	@Override
	public boolean isAvailable(String loginSign) {
		return this.dao.checkUsernameAvailable(loginSign);
	}
}
