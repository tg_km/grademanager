package ch.gms.application.login;

import java.sql.SQLException;

import ch.gms.application.util.Pair;

/*************************************************************************
 * Exception to be thrown when login fails
 * 
 * @author TIMG
 * @since 11.03.2016
 ************************************************************************/
public class LoginFailedException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param loginData The login data of the failed login in a pair container
	 ************************************************************************/
	public LoginFailedException(Pair<String, String> loginData){
		super("Login mit Benutzername \"" + loginData.getFirstValue() + "\" fehlgeschlagen");
	}
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param e The SQLException thrown by the IOManager to indicate that
	 * the login failure was caused by a database problem
	 ************************************************************************/
	public LoginFailedException(SQLException e){
		super("<html>Die Datenbank ist nicht erreichbar.<br> Überprüfen Sie Ihre Datenbankverbindung.</html>");
	}
	

}
