package ch.gms.application.login;


import ch.gms.application.Converter;
import ch.gms.application.businessObjects.BOUser;
import ch.gms.application.util.Pair;
import ch.gms.dto.UserDTO;
import ch.gms.persistence.DefaultDataAccessObject;
import ch.gms.persistence.DataAccessObject;

/****************************************************************
 * Implementation of the login handler interface
 *
 * @author TIMG
 * @since 16.03.2016
 ***************************************************************/
public class DefaultLoginService implements LoginService {
	
	private DataAccessObject dao;
	
	public DefaultLoginService() {
		this.dao = new DefaultDataAccessObject();
	}

	@Override
	public BOUser login(Pair<String, String> loginData) throws LoginFailedException{
		UserDTO userDto = this.dao.login(loginData.getFirstValue(), loginData.getSecondValue());
		
		if(userDto == null){
			throw new LoginFailedException(loginData);
		}
		return Converter.convertToBO(userDto);
	}
	
}

