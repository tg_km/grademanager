package ch.gms.application.login;

import ch.gms.application.businessObjects.BOUser;
import ch.gms.application.util.Pair;

/**************************************************************************
 * Interface to specify the methods required for login
 * 
 * @author TIMG
 * @since 10.03.2016
 *************************************************************************/
public interface LoginService {
	
	/*************************************************************************
	 * Checks if the given username/password combination is valid and returns
	 * the corresponding user object, if yes
	 * 
	 * @param loginData Pair holding the given username and password
	 * @return The found user. Can be null if no match has been made
	 * @throws LoginFailedException If no match has been made
	 ************************************************************************/
	public BOUser login(Pair<String, String> loginData) throws LoginFailedException;

}
