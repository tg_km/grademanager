package ch.gms.application.login;

import ch.gms.application.businessObjects.BOUser;
import ch.gms.application.util.Pair;

public class DummyLoginService implements LoginService {

	@Override
	public BOUser login(Pair<String, String> loginData) throws LoginFailedException {
		return new BOUser("Mustermann", "Max", "test", "test", true, -1);
	}

}
