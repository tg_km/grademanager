package ch.gms.application.businessObjects;

import java.util.ArrayList;

/*******************************************************************
 * Business class to represent a school class
 * 
 * @author TIMG/KENMAU
 * @since 07.03.2016
 *******************************************************************/
public class BOClass {
	
	private String name;
	private int id;
	private int lessonId;
	private ArrayList<BOStudent> students;
	private ArrayList<BOExam> exams;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param name The name of the class
	 * @param lessonId The lessonID of the class, is -1 when no lesson is
	 * 		  recognizable
	 * @param id The unique ID of the class
	 ************************************************************************/
	public BOClass(String name, int lessonId, int id){
		this.name = name;
		this.lessonId = lessonId;
		this.id = id;
		this.students = new ArrayList<>();
		this.exams = new ArrayList<>();
	}

	/**************************************************************************
	 * Gets the name.
	 *
	 * @return the name
	 *************************************************************************/
	public String getName() {
		return name;
	}

	/**************************************************************************
	 * Sets the name.
	 *
	 * @param name the new name
	 *************************************************************************/
	public void setName(String name) {
		this.name = name;
	}
	
	/**************************************************************************
	 * Gets the exams.
	 *
	 * @return the exams
	 **************************************************************************/
	public ArrayList<BOExam> getExams(){
		return this.exams;
	}
	
	/**************************************************************************
	 * Adds an exam to this class
	 *
	 * @param exam the exam to be added
	 *************************************************************************/
	public void addExam(BOExam exam){
		this.exams.add(exam);
	}

	/**************************************************************************
	 * Gets the students.
	 *
	 * @return the students
	 **************************************************************************/
	public ArrayList<BOStudent> getStudents() {
		return students;
	}
	
	/**************************************************************************
	 * Adds a student to this class.
	 *
	 * @param student the student to be added
	 **************************************************************************/
	public void addStudent(BOStudent student){
		this.students.add(student);
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return this.name;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return id;
	}

	/**************************************************************************
	 * Gets the lesson id.
	 *
	 * @return the lesson id
	 **************************************************************************/
	public int getLessonId() {
		return lessonId;
	}
	
	/**************************************************************************
	 * In order to reload the exams of a class, they need to be reset in the
	 * BO before it is converted to a DTO
	 **************************************************************************/
	public void resetExams(){
		this.exams = new ArrayList<>();
	}
	
}
