package ch.gms.application.businessObjects;

import java.util.ArrayList;

/*************************************************************************
 * Business class to represent a school teacher
 * 
 * @author TIMG/KENMAU
 * @since 07.03.2016
 ************************************************************************/
public class BOTeacher extends BOUser{
	
	private ArrayList<BOModule> modules;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param lastname The last name of the teacher
	 * @param firstname The first name of the teacher
	 * @param loginSign The login sign of the teacher
	 * @param password The password of the teacher
	 * @param loginId The unique Login ID of the teacher
	 ************************************************************************/
	public BOTeacher(String lastname, String firstname, String loginSign, String password, int loginId) {
		super(lastname, firstname, loginSign, password, false, loginId); //last parameter isAdmin set to false -> Teacher never is an admin
		this.modules = new ArrayList<>();
	}
	
	/*************************************************************************
	 * @see ch.gms.application.businessObjects.BOUser#getId()
	 *************************************************************************/
	public int getId(){
		return super.loginId;
	}
	
	/*************************************************************************
	 * @see ch.gms.application.businessObjects.BOUser#getLastname()
	 *************************************************************************/
	public String getLastname() {
		return super.lastname;
	}

	/*************************************************************************
	 * @see ch.gms.application.businessObjects.BOUser#setLastname(java.lang.String)
	 *************************************************************************/
	public void setLastname(String lastName) {
		super.lastname = lastName;
	}

	/*************************************************************************
	 * @see ch.gms.application.businessObjects.BOUser#getFirstname()
	 *************************************************************************/
	public String getFirstname() {
		return super.firstname;
	}

	/*************************************************************************
	 * @see ch.gms.application.businessObjects.BOUser#setFirstname(java.lang.String)
	 *************************************************************************/
	public void setFirstname(String firstname) {
		super.firstname = firstname;
	}
	
	/**************************************************************************
	 * Adds a module to this teacher
	 *
	 * @param modules the modules to be added
	 *************************************************************************/
	public void addModule(BOModule... modules){
		for(BOModule module : modules){
			this.modules.add(module);
		}
	}

	/**************************************************************************
	 * Gets the modules.
	 *
	 * @return the modules
	 **************************************************************************/
	public ArrayList<BOModule> getModules() {
		return modules;
	}
	
	/**************************************************************************
	 * Adds a class to a module of this teacher
	 *
	 * @param module the module to which the class should be added
	 * @param clazz the class to be added
	 **************************************************************************/
	public void addClassToModule(BOModule module, BOClass clazz){
		for(BOModule existingModule : this.modules){
			if(existingModule == module){
				existingModule.addClass(clazz);
			}
		}
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return this.firstname + " " + this.lastname;
	}
	
}
