package ch.gms.application.businessObjects;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/*************************************************************************
 * Business class to represent an exam
 * 
 * @author TIMG
 * @since 07.03.2016
 ************************************************************************/
public class BOExam {
	
	private String description;
	private String name;
	private Date date;
	private int id;
	private int lessonId;
	private Map<BOStudent, Double> grades;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param description The description of the exam
	 * @param name The name of the exam
	 * @param date The date on which the exam has taken place
	 * @param lessonId The unique lesson ID of the Exam
	 * @param id The unique ID of the Exam
	 ************************************************************************/
	public BOExam(String description, String name, Date date, int lessonId, int id){
		this.description = description;
		this.name = name;
		this.date = date;
		this.lessonId = lessonId;
		this.id = id;
		this.grades= new HashMap<>();
	}

	/**************************************************************************
	 * Gets the name.
	 *
	 * @return the name
	 **************************************************************************/
	public String getName() {
		return name;
	}

	/**************************************************************************
	 * Sets the name.
	 *
	 * @param name the new name
	 *************************************************************************/
	public void setName(String name) {
		this.name = name;
	}

	/**************************************************************************
	 * Gets the description.
	 *
	 * @return the description
	 **************************************************************************/
	public String getDescription() {
		return description;
	}

	/**************************************************************************
	 * Sets the description.
	 *
	 * @param description the new description
	 **************************************************************************/
	public void setDescription(String description) {
		this.description = description;
	}

	/**************************************************************************
	 * Gets the date.
	 *
	 * @return the date
	 **************************************************************************/
	public Date getDate() {
		return date;
	}
	
	/**************************************************************************
	 * Gets the grades.
	 * Do not use to get a single grade!
	 *  
	 * @See getGradeForStudent(BOStudent student)
	 * @return the grades
	 **************************************************************************/
	public Map<BOStudent, Double> getGrades(){
		return this.grades;
	}

	/**************************************************************************
	 * Adds a grade.
	 *
	 * @param student the student
	 * @param value the value
	 *************************************************************************/
	public void addGrade(BOStudent student, Double value){
		this.grades.put(student, value);
	}
	
	/**************************************************************************
	 * Gets the amount of grades.
	 *
	 * @return the amount of grades
	 **************************************************************************/
	public int getAmountOfGrades(){
		return this.grades.size();
	}
	
	/**************************************************************************
	 * Gets the amount of students who missed the exam of the given class
	 *
	 * @param clazz the class
	 * @return the missed
	 **************************************************************************/
	public int getMissed(BOClass clazz){
		return clazz.getStudents().size() - this.grades.size();
	}
	
	/**************************************************************************
	 * Gets the grade for a given student
	 *
	 * @param student the student
	 * @return the grade of the student
	 **************************************************************************/
	public double getGradeForStudent(BOStudent student){
		for(Map.Entry<BOStudent, Double> grade : this.grades.entrySet()){
			if(grade.getKey().getId() == student.getId()){
				return grade.getValue();
			}
		}
		return 0;
	}
	
	/**************************************************************************
	 * Update grade of a student
	 *
	 * @param student the student
	 * @param value the value
	 **************************************************************************/
	public void updateGrade(BOStudent student, double value){
		for(Map.Entry<BOStudent, Double> grade : this.grades.entrySet()){
			if(grade.getKey().getId() == student.getId()){
				grade.setValue(value);
				return;
			}
		}
		this.grades.put(student, value);
	}

	/**************************************************************************
	 * Gets the average of all grades in this exam
	 *
	 * @return the average
	 **************************************************************************/
	public double getAverage() {
		try{
			return this.grades.values().stream().mapToDouble(grade -> grade).average().orElse(0);
		}catch(NoSuchElementException nsee){
			return 0;
		}
	}

	/**************************************************************************
	 * Gets the lowest grade in the exam
	 *
	 * @return the lowest grade
	 **************************************************************************/
	public double getLowestGrade() {
		try{
			return Collections.min(this.grades.values());
		}catch(NoSuchElementException nsee){
			return 0;
		}
	}

	/**************************************************************************
	 * Gets the highest grade.
	 *
	 * @return the highest grade
	 **************************************************************************/
	public double getHighestGrade() {
		try{
			return Collections.max(this.grades.values());
		}catch(NoSuchElementException nsee){
			return 0;
		}
	}
	
	/**************************************************************************
	 * Gets the lesson id.
	 *
	 * @return the lesson id
	 **************************************************************************/
	public int getLessonId() {
		return lessonId;
	}
	
	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return id;
	}
}
