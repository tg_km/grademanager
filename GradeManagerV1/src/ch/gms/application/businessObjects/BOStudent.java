package ch.gms.application.businessObjects;

/*************************************************************************
 * Business class to represent a student
 * 
 * @author TIMG/KENMAU
 * @since 07.03.2016
 ************************************************************************/
public class BOStudent {
	
	private String firstname, lastname;
	private int id, classId;

	/*************************************************************************
	 * Constructor
	 * 
	 * @param firstName The first name of the student
	 * @param lastName The last name of the student
	 * @param id The unique ID of the student
	 * @param classId The unique ID of the class where the student belongs
	 ************************************************************************/
	public BOStudent(String firstName, String lastName, int id, int classId) {
		this.firstname = firstName;
		this.lastname = lastName;
		this.id = id;
		this.classId = classId;
	}

	/**************************************************************************
	 * Gets the first name.
	 *
	 * @return the first name
	 **************************************************************************/
	public String getFirstName() {
		return firstname;
	}

	/**************************************************************************
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 **************************************************************************/
	public void setFirstName(String firstName) {
		this.firstname = firstName;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return id;
	}

	/**************************************************************************
	 * Gets the last name.
	 *
	 * @return the last name
	 **************************************************************************/
	public String getLastName() {
		return lastname;
	}

	/**************************************************************************
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 **************************************************************************/
	public void setLastName(String lastName) {
		this.lastname = lastName;
	}
	
	/**************************************************************************
	 * Gets the class id.
	 *
	 * @return the class id
	 **************************************************************************/
	public int getClassId(){
		return this.classId;
	}
	
	/*************************************************************************
	 * @see java.lang.Object#toString()
	 *************************************************************************/
	@Override
	public String toString(){
		return this.firstname + " " + this.lastname;
	}
	
}
