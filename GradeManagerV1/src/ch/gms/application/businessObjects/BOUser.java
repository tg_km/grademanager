package ch.gms.application.businessObjects;

/*************************************************************************
 * Business class to represent a user, as a person who can log in
 * 
 * @author TIMG/KENMAU
 * @since 07.03.2016
 ************************************************************************/
public class BOUser {
	
	protected String firstname, lastname, loginSign, password;
	protected int loginId;
	protected boolean isAdmin;
	
	/*************************************************************************
	 * Constructor
	 * 
	 * @param lastname The last name of the user
	 * @param firstname The first name of the user
	 * @param loginSign The login sign of the user
	 * @param password The password of the user
	 * @param isAdmin Determines if user has administrator privileges
	 * @param loginId The unique login ID of the user
	 ************************************************************************/
	public BOUser(String lastname, String firstname, String loginSign, String password, boolean isAdmin, int loginId){
		this.firstname = firstname;
		this.lastname = lastname;
		this.loginSign = loginSign;
		this.password = password;
		this.isAdmin = isAdmin;
		this.loginId = loginId;
	}

	/**************************************************************************
	 * Gets the firstname.
	 *
	 * @return the firstname
	 **************************************************************************/
	public String getFirstname() {
		return firstname;
	}

	/****************************************************************************
	 * Sets the firstname.
	 *
	 * @param firstname the new firstname
	 ***************************************************************************/
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**************************************************************************
	 * Gets the lastname.
	 *
	 * @return the lastname
	 **************************************************************************/
	public String getLastname() {
		return lastname;
	}

	/**************************************************************************
	 * Sets the lastname.
	 *
	 * @param lastname the new lastname
	 **************************************************************************/
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**************************************************************************
	 * Gets the login sign.
	 *
	 * @return the login sign
	 **************************************************************************/
	public String getLoginSign() {
		return loginSign;
	}

	/**************************************************************************
	 * Sets the login sign.
	 *
	 * @param loginSign the new login sign
	 **************************************************************************/
	public void setLoginSign(String loginSign) {
		this.loginSign = loginSign;
	}

	/**************************************************************************
	 * Gets the password.
	 *
	 * @return the password
	 **************************************************************************/
	public String getPassword() {
		return password;
	}

	/**************************************************************************
	 * Sets the password.
	 *
	 * @param password the new password
	 **************************************************************************/
	public void setPassword(String password) {
		this.password = password;
	}

	/****************************************************************************
	 * Checks if this user has admin privileges
	 *
	 * @return true, if this user has admin privileges
	 ***************************************************************************/
	public boolean isAdmin() {
		return isAdmin;
	}

	/**************************************************************************
	 * Declares if this user has admin privileges
	 *
	 * @param isAdmin true, if this user has admin privileges 
	 **************************************************************************/
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	/**************************************************************************
	 * Gets the id.
	 *
	 * @return the id
	 **************************************************************************/
	public int getId() {
		return loginId;
	}
}
