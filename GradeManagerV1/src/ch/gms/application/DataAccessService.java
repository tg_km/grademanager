package ch.gms.application;

import java.util.ArrayList;

import ch.gms.application.businessObjects.BOClass;
import ch.gms.application.businessObjects.BOModule;
import ch.gms.application.businessObjects.BOTeacher;
import ch.gms.dto.GmsDTO;

/*************************************************************************
 * Interface that defines all methods required for access of
 * the database and storing data in the db
 * 
 * @author TIMG
 * @since 10.03.2016
 ************************************************************************/
public interface DataAccessService {
	
	/*************************************************************************
	 * Adds all the exam data to an existing teacher business object
	 * 
	 * @param teacher The teacher to who the exams out of the database should
	 * 		  be added.
	 * @return The same teacher with exam data added
	 ************************************************************************/
	public BOTeacher addExamsToTeacher(BOTeacher teacher);
	
	/*************************************************************************
	 * Gets a teacher business object by its login ID
	 * 
	 * @param loginId The ID belonging to the desired teacher
	 * @return The found teacher with given ID
	 ************************************************************************/
	public BOTeacher getTeacherByLoginIdWOExam(int loginId);
	
	/*************************************************************************
	 * Gets all teacher business objects.
	 * 
	 * @return The found teachers
	 ************************************************************************/
	public ArrayList<BOTeacher> getAllTeachers();
	
	/*************************************************************************
	 * Gets all class business objects. Holds only the class data without
	 * their children.
	 * 
	 * @return The found classes
	 ************************************************************************/
	public ArrayList<BOClass> getAllClasses();
	
	/*************************************************************************
	 * Gets all module business object. Holds only the module data without
	 * their children.
	 * 
	 * @return The found modules
	 ************************************************************************/
	public ArrayList<BOModule> getAllModules();
	
	/*************************************************************************
	 * Gets all class business objects including their children, but without
	 * their exams
	 * 
	 * @return The found classes
	 ************************************************************************/
	public ArrayList<BOClass> getAllClassesWithStudents();
	
	/*************************************************************************
	 * Saves a IGMSDTO
	 * 
	 * @param dto The DTO to be saved in the database
	 * @return True if the DTO could be saved correctly
	 ************************************************************************/
	public boolean save(GmsDTO dto);
	
	/*************************************************************************
	 * Checks if a username is available in the database, as one username
	 * can only exists once.
	 * 
	 * @param loginSign The login sign to be tested for uniqueness
	 * @return True if the 
	 ************************************************************************/
	public boolean isAvailable(String loginSign);
	
}
