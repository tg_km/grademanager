package ch.gms.persistence;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/*************************************************************************
 * Only class to open a connection to the database. Handles connection
 * creation and specifies connection credentials and other information
 * 
 * @author KENMAU
 * @since 07.03.2016
 ************************************************************************/
public class IOManager {
	
	private static IOManager theInstance;
	
	private HikariConfig config;
	private  HikariDataSource ds;
	
	/*************************************************************************
	 * Gets the only instance of the IOManager
	 * If it's not built already, a new Manager will be built including
	 * the connection pool supplying the connections
	 * 
	 * @return The IOManager instance
	 ************************************************************************/
	public static IOManager getInstance(){
		if(theInstance == null){
			theInstance = new IOManager();
		}
		return theInstance;
	}
	
	/*************************************************************************
	 * Private constructor
	 ************************************************************************/
	private IOManager(){
		config = new HikariConfig("resources/config_hikari.properties");
		config.setMaximumPoolSize(5);
		config.setConnectionTimeout(5*1000); //in ms
		config.setInitializationFailFast(false);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		
		ds = new HikariDataSource(config);
	}
	
	/*************************************************************************
	 * Gets a connection to the database with the specified information
	 * 
	 * @return A database connection
	 * @throws An SQLException when the connection couldn't be established 
	 ************************************************************************/
	public Connection getConnection() throws SQLException{
		return ds.getConnection();
	}
	
}