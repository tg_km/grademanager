package ch.gms.persistence;

import java.util.ArrayList;

import ch.gms.application.login.LoginFailedException;
import ch.gms.dto.ClassDTO;
import ch.gms.dto.GmsDTO;
import ch.gms.dto.ModuleDTO;
import ch.gms.dto.TeacherDTO;
import ch.gms.dto.UserDTO;

/*************************************************************************
 * Interface to specify all methods needed for db access
 * 
 * @author KENMAU
 * @since 07.03.2016
 ************************************************************************/
public interface DataAccessObject {
	
	/***********************************************************************
	 * Fetches the data for the user from the database   
	 * 
	 * @param The loginsign and the password of the desired user
	 * @return A UserDTO with the matching data
	 * @throws A LoginFailedException when the database is unavailable
	 **********************************************************************/
	public UserDTO login(String loginSign, String pw) throws LoginFailedException;
	
	/***********************************************************************
	 * Adds exam data from the database to an existing teacher dto
	 * 
	 * @return the teacher dto with its exams
	 **********************************************************************/
	public TeacherDTO addExamsToTeacher(TeacherDTO teacher);
	
	/***********************************************************************
	 * Gets a teacher by its login sign without the exam data
	 * 
	 * @return the found teacher
	 **********************************************************************/
	public TeacherDTO getTeacherByLoginIdWOExam(int loginId);
	
	/***********************************************************************
	 * Gets all teachers from the database
	 * 
	 * @return all teachers from the database
	 **********************************************************************/
	public ArrayList<TeacherDTO> getAllTeachers();
	
	/***********************************************************************
	 * Gets all classes from the database without the corresponding
	 * students
	 * 
	 * @return all classes from the database
	 **********************************************************************/
	public ArrayList<ClassDTO> getAllClasses();
	
	/***********************************************************************
	 * Gets all classes from the database including their students
	 * 
	 * @return all classes from the database
	 **********************************************************************/
	public ArrayList<ClassDTO> getAllClassesWithStudents();
	
	/***********************************************************************
	 * Gets all modules from the database
	 * 
	 * @return all modules from the database
	 **********************************************************************/
	public ArrayList<ModuleDTO> getAllModules();
	
	/***********************************************************************
	 * Saves a DTO to the database. If there is already an existing entry,
	 * it will be updated
	 * 
	 * @return True, if the save was successful
	 **********************************************************************/
	public boolean save(GmsDTO dto);
	
	/***********************************************************************
	 * Gets all users from the database which have admin privileges
	 * 
	 * @return all admin users from the database
	 **********************************************************************/
	public ArrayList<UserDTO> getAllAdminUsers();
	
	/***********************************************************************
	 * Checks if a Username is available   
	 * 
	 * @param The to be tested loginsign 
	 * @return true if username is free
	 **********************************************************************/
	public boolean checkUsernameAvailable(String loginSign);

}
