package ch.gms.persistence;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import ch.gms.application.login.LoginFailedException;
import ch.gms.dto.ClassDTO;
import ch.gms.dto.ExamDTO;
import ch.gms.dto.GmsDTO;
import ch.gms.dto.ModuleDTO;
import ch.gms.dto.StudentDTO;
import ch.gms.dto.TeacherDTO;
import ch.gms.dto.UserDTO;

/*************************************************************************
 * Implementation of the data access interface
 * 
 * @author KENMAU
 * @since 07.03.2016
 ************************************************************************/
public class DefaultDataAccessObject implements DataAccessObject {

	private IOManager iom;
	
	/***********************************************************************
	 * Constructor
	 **********************************************************************/
	public DefaultDataAccessObject() {
		iom = IOManager.getInstance();
	}

	@Override
	public boolean checkUsernameAvailable(String loginSign){
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT LOGINNAME FROM login WHERE LOGINNAME = ?");
			statement.setString(1, loginSign);
			ResultSet result = statement.executeQuery(); 
			if(result.next()){
				System.out.println("username already taken");
				connection.close();
				return false;
			} else {
				System.out.println("no user found");
				connection.close();
				return true;
			}
		} catch (SQLException e) {
			System.err.println("Failed to check username availability: " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return false;
	}

	@Override
	public UserDTO login(String loginSign, String password) throws LoginFailedException{
		String firstname;
		String lastname;
		String pw = getHashedString(password);
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT ID, ADMIN FROM login WHERE loginname = ? AND passwort = ?");
			statement.setString(1, loginSign);
			statement.setString(2, pw);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				int login_id = result.getInt(1);
				boolean isAdmin = result.getBoolean(2);
				if (isAdmin) {
					PreparedStatement adminStatement = connection.prepareStatement("SELECT NAME,VORNAME FROM admin WHERE login_id = ?");
					adminStatement.setInt(1, login_id);
					ResultSet adminResult = adminStatement.executeQuery();
					adminResult.next();
					firstname = adminResult.getString(2);
					lastname = adminResult.getString(1);
					UserDTO admin = new UserDTO(firstname, lastname, loginSign, pw, true, login_id);
					connection.close();
					return admin;
					
				} else {
					PreparedStatement teacherStatement = connection.prepareStatement("SELECT NAME,VORNAME FROM lehrer WHERE login_id = ?");
					teacherStatement.setInt(1, login_id);
					ResultSet teacherResult = teacherStatement.executeQuery();
					teacherResult.next();
					firstname = teacherResult.getString(2);
					lastname = teacherResult.getString(1);
					UserDTO user = new UserDTO(firstname, lastname, loginSign, pw, false, login_id);
					connection.close();
					return user;
				}
			} else {
				return null;
			}
		} catch (SQLException sqle) {
			System.err.println("Failed to login: " + sqle.getMessage());
			throw new LoginFailedException(sqle);
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
	}

	@Override
	public TeacherDTO addExamsToTeacher(TeacherDTO teacher) {
		TeacherDTO t = teacher;
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement examStatement = connection.prepareStatement("SELECT ID, NAME, KURZBESCHREIBUNG, DATUM FROM pruefung WHERE UNTERRICHT_ID = ?");

			ArrayList<ModuleDTO> modules = t.getModules();
			for (ModuleDTO module : modules) {
				ArrayList<ClassDTO> classes = module.getClasses();
				for (ClassDTO clazz : classes) {
					int lessonId = clazz.getLessonId();
					ArrayList<StudentDTO> students = clazz.getStudents();
					examStatement.setInt(1, lessonId);
					ResultSet result = examStatement.executeQuery();
					while (result.next()) {
						int examId = result.getInt(1);
						String examName = result.getString(2);
						String examDesc = result.getString(3);
						Date examDate = result.getTimestamp(4);
						ExamDTO exam = new ExamDTO(examDesc, examName, examDate, lessonId, examId);
						for (StudentDTO student : students) {
							int studId = student.getId();
							PreparedStatement gradeStatement = connection.prepareStatement("SELECT NOTE FROM schuelerpruefungsassoziation WHERE PRUEFUNG_ID = ? AND SCHUELER_ID =  ? ");
							gradeStatement.setInt(1, examId);
							gradeStatement.setInt(2, studId);
							ResultSet result2 = gradeStatement.executeQuery();
							while (result2.next()) {
								Double grade = result2.getDouble(1);
								System.out.println("[" + new Date() + "] " + "Note: '" + grade + " von Sch�ler mit ID " + studId + "' gefunden");
								exam.addGrade(student, grade);
							}
						}
						clazz.addExam(exam);
					}
				}
			}
			connection.close();
			return t;

		} catch (SQLException sqle) {
			System.err.println("Failed to load exams: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public TeacherDTO getTeacherByLoginIdWOExam(int loginId) {
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement teacherStatement = connection.prepareStatement("SELECT lehrer.VORNAME, lehrer.NAME, login.LOGINNAME, login.PASSWORT, login.ID, lehrer.ID FROM lehrer INNER JOIN LOGIN ON lehrer.LOGIN_ID = LOGIN.ID WHERE login.ID=?");
			PreparedStatement moduleStatement = connection.prepareStatement("SELECT MODULNUMMER, MODULNAME, MODULBESCHREIBUNG, MODUL_ID FROM schuluebersicht WHERE LEHRER_ID= ? GROUP BY MODUL_ID");
			PreparedStatement classStatement = connection.prepareStatement("SELECT KLASSE, KLASSE_ID, ID FROM schuluebersicht WHERE LEHRER_ID = ?  AND MODUL_ID = ?");
			PreparedStatement studentStatement = connection.prepareStatement("SELECT VORNAME, NAME, ID, KLASSE_ID FROM schueler WHERE KLASSE_ID=?");

			teacherStatement.setInt(1, loginId);		
			ResultSet teacherResult = teacherStatement.executeQuery();					
			if (teacherResult.next()) {
				String teacherFirstname = teacherResult.getString(1);
				String teacherLastname = teacherResult.getString(2);
				String loginSign = teacherResult.getString(3);
				String pw = teacherResult.getString(4);
				int teacherLoginId = teacherResult.getInt(5);
				int teacherId = teacherResult.getInt(6);
				System.out.println(
						"[" + new Date() + "] " + "Lehrer '" + teacherFirstname + " " + teacherLastname + "' gefunden");
				moduleStatement.setInt(1, teacherId);
				ResultSet moduleResult = moduleStatement.executeQuery();
				ArrayList<ModuleDTO> modules = new ArrayList<ModuleDTO>();
				while (moduleResult.next()) {
					String modnumber = moduleResult.getString(1);
					String modname = moduleResult.getString(2);
					String moddesc = moduleResult.getString(3);
					int modulId = moduleResult.getInt(4);
					System.out.println("[" + new Date() + "] " + "Modul '" + modnumber + " " + moddesc + "' gefunden");
					ModuleDTO moduleSchoolClass = new ModuleDTO(modnumber, modname, moddesc, modulId);
					classStatement.setInt(1, teacherId);
					classStatement.setInt(2, modulId);
					ResultSet classResult = classStatement.executeQuery();
					while (classResult.next()) {
						String cldesc = classResult.getString(1);
						int classId = classResult.getInt(2);
						int lessonId = classResult.getInt(3);
						System.out.println("[" + new Date() + "] " + "Klasse '" + cldesc + "' gefunden");
						ClassDTO schoolClass = new ClassDTO(cldesc, lessonId, classId);
						studentStatement.setInt(1, classId);
						ResultSet studentResult = studentStatement.executeQuery();

						while (studentResult.next()) {
							String firstname = studentResult.getString(1);
							String lastname = studentResult.getString(2);
							int sId = studentResult.getInt(3);
							System.out.println(
									"[" + new Date() + "] " + "Sch�ler '" + firstname + " " + lastname + "' gefunden");
							StudentDTO student = new StudentDTO(firstname, lastname, sId, classId);
							schoolClass.addStudent(student);
						}
						moduleSchoolClass.addClass(schoolClass);

						if (!studentResult.next()) {
							studentResult.close();
						}
					}
					if (!classResult.next()) {
						classResult.close();
						modules.add(moduleSchoolClass);
					}
				}
				TeacherDTO teacher = new TeacherDTO(teacherFirstname, teacherLastname, loginSign, pw, teacherLoginId, modules);
				connection.close();
				return teacher;
			} else {
				return null;
			}
		} catch (SQLException sqle) {
			System.err.println("Failed to load teacher: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public ArrayList<TeacherDTO> getAllTeachers() {
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT LOGIN_ID FROM lehrer");
			ResultSet result = statement.executeQuery();
			ArrayList<TeacherDTO> teachers = new ArrayList<TeacherDTO>();
			while (result.next()) {
				int loginId = result.getInt(1);
				teachers.add(getTeacherByLoginIdWOExam(loginId));
				if (result.isLast()) {
					connection.close();
					return teachers;
				}
			}

		} catch (SQLException sqle) {
			System.err.println("Failed to load teachers: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public boolean save(GmsDTO dto) {
		if (dto instanceof TeacherDTO) {
			return saveTeacher((TeacherDTO) dto);
		} else if (dto instanceof StudentDTO) {
			return saveStudent((StudentDTO) dto);
		} else if (dto instanceof ModuleDTO) {
			return saveModule((ModuleDTO) dto);
		} else if (dto instanceof ClassDTO) {
			return saveClass((ClassDTO) dto);
		}
		return false;
	}

	private boolean saveTeacher(TeacherDTO dto) {
		boolean success = true;
		TeacherDTO teacher = dto;
		String login = teacher.getLoginSign();
		String pw = getHashedString(teacher.getPassword());
		String firstname = teacher.getFirstname();
		String lastname = teacher.getLastname();
		int id = 0;
		Connection connection = null;
		ArrayList<ModuleDTO> modules = teacher.getModules();
		try {
			connection = iom.getConnection();
			PreparedStatement statementLogin = connection.prepareStatement("INSERT INTO login(LOGINNAME, PASSWORT, ADMIN) VALUES(?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			statementLogin.setString(1, login);
			statementLogin.setString(2, pw);
			statementLogin.setBoolean(3, false);
			statementLogin.executeUpdate();
			ResultSet rs = statementLogin.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getInt(1);
			}
			PreparedStatement statementTeacher = connection.prepareStatement("INSERT INTO lehrer(NAME, VORNAME, LOGIN_ID) VALUES(?,?,?)" , PreparedStatement.RETURN_GENERATED_KEYS);
			statementTeacher.setString(1, lastname);
			statementTeacher.setString(2,firstname);
			statementTeacher.setInt(3,id);
			statementTeacher.executeUpdate();
			PreparedStatement statementInsModClsTeach = connection.prepareStatement("INSERT INTO unterricht(LEHRER_ID, KLASSE_ID, MODUL_ID) VALUES(?,?,?)");
			rs = statementTeacher.getGeneratedKeys();
			if (rs.next()) {
				int teacherId = rs.getInt(1);
				for (ModuleDTO module : modules) {
					int modId = module.getId();
					ArrayList<ClassDTO> classes = module.getClasses();
					for (ClassDTO moduleClass : classes) {
						int classId = moduleClass.getId();
						statementInsModClsTeach.setInt(1,teacherId);
						statementInsModClsTeach.setInt(2,classId);
						statementInsModClsTeach.setInt(3,modId);
						statementInsModClsTeach.executeUpdate();
					}
				}
			}
			connection.close();
		} catch (SQLException sqle) {
			System.err.println("Failed to save teacher: " + sqle.getMessage());
			sqle.printStackTrace();
			success = false;
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return success;
	}

	private boolean saveStudent(StudentDTO student) {
		boolean success = true;
		int classId = student.getClassId();
		String sFirstName = student.getFirstName();
		String sLastName = student.getLastName();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("INSERT INTO schueler(NAME, VORNAME, KLASSE_ID) VALUES(?,?,?)");
			statement.setString(1, sLastName);
			statement.setString(2, sFirstName);
			statement.setInt(3, classId);
			statement.executeUpdate();
			connection.close();
		} catch (SQLException sqle) {
			System.err.println("Failed to save student: " + sqle.getMessage());
			sqle.printStackTrace();
			success = false;
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return success;
	}

	private boolean saveModule(ModuleDTO dto) {
		boolean success = true;
		ModuleDTO module = dto;
		String modNumber = module.getNumber();
		String modName = module.getName();
		String modulDesc = module.getDescription();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("INSERT INTO modul(BEZEICHNUNG, NAME, KURZBESCHREIBUNG) VALUES(?,?,?)");
			statement.setString(1, modNumber);
			statement.setString(2, modName);
			statement.setString(3, modulDesc);			
			statement.executeUpdate();
			connection.close();
		} catch (SQLException sqle) {
			System.err.println("Failed to save module: " + sqle.getMessage());
			sqle.printStackTrace();
			success = false;
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return success;
	}

	private boolean saveExam(ExamDTO dto) {
		boolean success = true;
		ExamDTO exam = dto;
		String desc = exam.getDescription();
		String name = exam.getName();
		int uId = exam.getLessonId();
		int id = exam.getId();
		java.sql.Date date = new java.sql.Date(exam.getDate().getTime());
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO pruefung(ID, NAME, KURZBESCHREIBUNG, DATUM, UNTERRICHT_ID) VALUES(?,?,?,?,?)"
					+ " ON DUPLICATE KEY UPDATE NAME=VALUES(NAME), KURZBESCHREIBUNG=VALUES(KURZBESCHREIBUNG),"
					+ "DATUM=VALUES(DATUM), UNTERRICHT_ID=VALUES(UNTERRICHT_ID)", PreparedStatement.RETURN_GENERATED_KEYS);
			PreparedStatement statementGrades = connection.prepareStatement(
					"INSERT INTO schuelerpruefungsassoziation(PRUEFUNG_ID, SCHUELER_ID, NOTE) VALUES(?,?,?)"
					+ " ON DUPLICATE KEY UPDATE NOTE=VALUES(NOTE);");
			
			if(id >= 1){
				statement.setInt(1, id);
			} else {
				statement.setInt(1, 0);
			}
			statement.setString(2, name);
			statement.setString(3, desc);
			statement.setDate(4, date);
			statement.setInt(5, uId);
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()){
				id = rs.getInt(1);
			} 
				for(Map.Entry<StudentDTO, Double> entry : exam.getGrades().entrySet()){
					double grade = entry.getValue();
					int studentId = entry.getKey().getId();
					statementGrades.setInt(1, id);
					statementGrades.setInt(2, studentId);
					statementGrades.setDouble(3, grade);
					System.out.println(statementGrades);
					statementGrades.executeUpdate();
				}
			connection.close();
		} catch (SQLException sqle) {
			System.err.println("Failed to save exam: " + sqle.getMessage());
			sqle.printStackTrace();
			success = false;
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return success;
	}

	private boolean saveClass(ClassDTO dto) {
		boolean success = true;
		
		if (dto.getExams().size() > 0) {
			for (ExamDTO exam : dto.getExams()) {
				if(success == false){ //it should not be possible to change from false to true
					saveExam(exam);
				}else{
					success = saveExam(exam);
				}
			}
			return success;
		} else {
			Connection connection = null;
			try {
				connection = iom.getConnection();
				PreparedStatement statement;
				String className = dto.getName();
				statement = connection.prepareStatement("INSERT INTO klasse(BEZEICHNUNG) VALUES(?)");
				statement.setString(1, className);
				statement.executeUpdate();
			} catch (SQLException e) {
				System.err.println("Failed to save exam: " + e.getMessage());
				e.printStackTrace();
				success = false;
			}finally{
				try {
					if(connection != null){
						connection.close();
					}
				} catch (SQLException e) {
					//do nothing as we are already in exceptional state
				}
			}
			return success;
		}
	}

	@Override
	public ArrayList<ClassDTO> getAllClasses() {
		String name;
		int id;
		ArrayList<ClassDTO> classArray = new ArrayList<ClassDTO>();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT ID, BEZEICHNUNG FROM klasse ORDER BY BEZEICHNUNG");
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				id = result.getInt(1);
				name = result.getString(2);
				ClassDTO schoolClass = new ClassDTO(name, -1, id);
				classArray.add(schoolClass);
				if (result.isLast()) {
					return classArray;
				}
			}
			connection.close();
		} catch (SQLException sqle) {
			System.err.println("Failed to load classes: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public ArrayList<ModuleDTO> getAllModules() {
		String number;
		String name;
		String description;
		int id;
		ArrayList<ModuleDTO> moduleArray = new ArrayList<ModuleDTO>();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT BEZEICHNUNG, NAME, KURZBESCHREIBUNG, ID FROM modul ORDER BY BEZEICHNUNG");
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				number = result.getString(1);
				name = result.getString(2);
				description = result.getString(3);
				id = result.getInt(4);
				ModuleDTO module = new ModuleDTO(number, name, description, id);
				moduleArray.add(module);
				if (result.isLast()) {
					connection.close();
					return moduleArray;
				}
			}
		} catch (SQLException sqle) {
			System.err.println("Failed to load modules: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public ArrayList<UserDTO> getAllAdminUsers() {
		String firstname, lastname, loginSign, password;
		int id;
		ArrayList<UserDTO> admins = new ArrayList<UserDTO>();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT login.LOGINNAME, login.PASSWORT, admin.VORNAME, admin.NAME, login.ID FROM admin INNER JOIN LOGIN ON admin.LOGIN_ID = LOGIN.ID WHERE login.ADMIN = 1;");
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				firstname = result.getString(3);
				lastname = result.getString(4);
				loginSign = result.getString(1);
				password = result.getString(2);
				id = result.getInt(5);

				UserDTO user = new UserDTO(firstname, lastname, loginSign, password, true, id);
				admins.add(user);
				if (result.isLast()) {
					connection.close();
					return admins;
				}
			}
		} catch (SQLException sqle) {
			System.err.println("Failed to load admin users: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return null;
	}

	@Override
	public ArrayList<ClassDTO> getAllClassesWithStudents() {
		String name, firstName, lastName;
		int id, studentId;
		ArrayList<ClassDTO> classArray = new ArrayList<ClassDTO>();
		Connection connection = null;
		try {
			connection = iom.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT ID, BEZEICHNUNG FROM klasse");
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				id = result.getInt(1);
				name = result.getString(2);
				ClassDTO schoolClass = new ClassDTO(name, -1, id);
				PreparedStatement statement2 = connection.prepareStatement("SELECT ID, NAME, VORNAME FROM schueler WHERE KLASSE_ID = ?");
				statement2.setInt(1, id);
				ResultSet result2 = statement2.executeQuery();
				while (result2.next()) {
					firstName = result2.getString(3);
					lastName = result2.getString(2);
					studentId = result2.getInt(1);
					StudentDTO student = new StudentDTO(firstName, lastName, studentId, id);
					schoolClass.addStudent(student);
				}
				classArray.add(schoolClass);
			}
		} catch (SQLException sqle) {
			System.err.println("Failed to load classes: " + sqle.getMessage());
			sqle.printStackTrace();
		}finally{
			try {
				if(connection != null){
					connection.close();
				}
			} catch (SQLException e) {
				//do nothing as we are already in exceptional state
			}
		}
		return classArray;
	}

	
	private String getHashedString(String unhashedString) {
		String hashedString = null;
			
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] b = unhashedString.getBytes();
			byte[] md5bytes = md.digest(b);
			hashedString = getHexaString(md5bytes);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return hashedString;	
	}
	
	private String getHexaString(byte[] byteArr){
		final char[] hexArray = "0123456789ABCDEF".toCharArray(); 
		char[] hexChars = new char[byteArr.length * 2];
		    for ( int j = 0; j < byteArr.length; j++ ) {
		        int v = byteArr[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
		        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		    }
		    return new String(hexChars);
	}
}
