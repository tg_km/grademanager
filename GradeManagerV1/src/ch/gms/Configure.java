package ch.gms;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Properties;

import ch.gms.dto.UserDTO;
import ch.gms.persistence.DefaultDataAccessObject;
import ch.gms.persistence.IOManager;

/*************************************************************************
 * Small console application to configure different settings of the
 * GMS Application
 * 
 * @author TIMG
 * @since 20.05.2016
 ************************************************************************/
public class Configure {
	
	BufferedReader in;
	DefaultDataAccessObject dao;

	public static void main(String[] args) {
		Configure config = new Configure();
		
		config.mainMenu();
	}
	
	private void print(String str){
		System.out.println(str);
	}
	
	private String readLine(){
		try {
			return this.in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void mainMenu(){
		this.in = new BufferedReader(new InputStreamReader(System.in));
		this.dao = new DefaultDataAccessObject();
		
		String ch = "";
		do{
			print("");
			print("#############################################################");
			print("#                   GMS Configure Server                    #");		
			print("#############################################################");
			print("");
			print("1 - Test database connection");
			print("2 - Show configuration");
			print("3 - Configure database");
			print("X - Exit");
			print("");
			
			ch = readLine().toUpperCase();
			switch(ch){
				case "1":
					testConnection();
					break;
				case "2":
					showConfig();
					break;
				case "3":
					configureDb();
					break;
			}
			
		}while(!ch.equals("X"));
		System.out.println("Bye!");
	}
	
	private boolean testConnection() {
		IOManager iom = IOManager.getInstance();
		try {
			Connection con = iom.getConnection();
			if(con != null){
				print("Connection to database established successfully!");
				return true;
			}
		} catch (Exception e) {
			System.err.println("Failed to make connection to database");
			System.err.println("Reconfigure your database!");
			print("");
		}
		return false;
	}

	private void configureDb() {
		print("DB Host:");
		String dbHost = readLine();
		print("DB Port:");
		String dbPort = readLine();
		print("DB User:");
		String dbUser = readLine();
		print("DB Password:");
		String dbPass = readLine();
		
		Properties prop = loadProperties();
		prop.put("dataSource.serverName", dbHost);
		prop.put("dataSource.portNumber", dbPort);
		prop.put("dataSource.user", dbUser);
		prop.put("dataSource.password", dbPass);
		storeProperties(prop);
		
		print("Saved!");
	}

	private void showConfig(){
		Properties props = loadProperties();
		print("");
		print("Database Configuration");
		print("-----------------------------------------");
		print("Database Host:                " + props.getProperty("dataSource.serverName"));
		print("Database Port:                " + props.getProperty("dataSource.portNumber"));
		print("Database User:                " + props.getProperty("dataSource.user"));
		print("Database Password:            " + props.getProperty("dataSource.password"));
		print("");
		print("Administrator Logins");
		print("-----------------------------------------");
		
		if(testConnection()){
			for(UserDTO user : this.dao.getAllAdminUsers()){
				print("Login:              " + user.getLoginSign());
				print("Password (hashed):  " + user.getPassword());
				print("");
			}
		}
		print("");
		
	}
	
	private Properties loadProperties(){
		InputStream input = null;
		
		try{
			input = new FileInputStream("resources/config_hikari.properties");
			Properties props = new Properties();
			props.load(input);
			return props;
		}catch(IOException e){
			System.err.println("Config file not found");
			e.printStackTrace();
		}finally{
			if(input != null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	private void storeProperties(Properties props){
		OutputStream output = null;
		
		try {
			output = new FileOutputStream("resources/config_hikari.properties");
			props.store(output, null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				output.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
	}

}
